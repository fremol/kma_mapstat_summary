

# kma_mapstat_summary

## Getting Started #

Clone the working branch:
```
git clone git@bitbucket.org:fremol/kma_mapstat_summary.git
```

## Introduction #

NB: All links to the CGE Wikipedia below can only be accessed on DTU or via a VPN, and require password - contact Thomas Nordahl if you need access.

kma_mapstat_summary is a CGE tool that summarizes mapped/aligned fragmentCounts from [.mapstat files](http://wiki.cge.dtu.dk:10090/display/CWP/mapstat) (output from [KMA](https://bitbucket.org/genomicepidemiology/kma/src/master/)) and produces abundance tables.  
The program can produce CLR/FPKM/TPM-normalized taxonomy-annotated abundance tables using .mapstat and .refdata files [.refdata files](http://wiki.cge.dtu.dk:10090/display/CWP/refdata).

Source code:
  - https://bitbucket.org/fremol/kma_mapstat_summary/src/master/
  
## Workflow overview
1. Obtain mapstat files from KMA mapping
2. Obtain .refdata files using the mapstat2refdata http://wiki.cge.dtu.dk:10090/display/CWP/refdata (only works for annotated databases, see below)
3. Run kma_mapstat_summary.py on the mapstat and refdata files to produce abundance tables


## Examples of usage

Basic example with no .refdata (will not annotate taxonomy): 
  
```
python3 kma_mapstat_summary.py -i /path/to/mapstat_directory/ -o output/dir -db <database_name> -v
```

Example with .refdata data and a count normalization file.
Will annotate taxonomy on a genus-level and produce CLR, TPM and FPKM adjusted tables:
  
```
python3 kma_mapstat_summary.py -i /path/to/mapstat_directory/ -ref /path/to/refdata_directory/ -cnf count_norm_file.txt -o output/dir -db <database_name> -tax genus -fpkm -tpm -clr -v
```

Example using mapstat files from a custom database (files specified with -i), running verbose, and outputting to a logfile:
 
```
python3 kma_mapstat_summary.py -i /path/to/mapstat_directory/ -o output/dir -cdb -clr -v -l logfile.txt
```

## Output
### kma_mapstat_summary directory
Depending on which parameters the program is run with, this directory will contain abundance on different taxonomical levels tables containing:
1. Raw fragmentCounts or aligned fragmentCountAln 
2. Sequencing-depth adjusted abundance (relative abundance)
3. CLR transformed abundance using https://bitbucket.org/genomicepidemiology/pycoda/src/master/
4. FPKM/TPM normalized abundance


## How to obtain .refdata files

The .refdata files can be made for the KMA databases that have been taxonomically annotated in our MySQL taxonomy table.
The KMA-indexed databases are listed under /home/databases/metagenomics/db/.
The ones that have full or partial taxonomy are:
genomic2 - taxonomy available for bacteria, bacteria_draft, fungi, protozoa, parasite, archaea, human, imgvr, kvit
kvI - taxonomy available for IMGVR, kvit, virus
Silva
Mitochondrion
Plastid

The .refdata files are made after KMA has finished.
To make them, run mapstat2refdata (C2 location: /home/projects/cge/apps/mapstat2refdata.pl) on the mapstat output files from KMA, like this example with Silva:
  
```
perl /home/projects/cge/apps/mapstat2refdata.pl -i /home/projects/cge/data/projects/1250/KMA/output/Silva_20200116/ -l /home/projects/cge/data/projects/1250/KMA/CROF_output/Silva_20200116_mapstat2refdata_log.txt -v -C -o /home/projects/cge/data/projects/1250/KMA/refdata_output/
```

## How to use count normalization file to obtain FPKM tables for ResFinder (or other databases)

The count normalization file contains two columns containing a sample ID and a number against which the read counts should be scaled, like this:
DTU_20201_MG_1000001_1_SampleName <tab> 12345678

FPKM is calculated like this: 

  ![\Large FPKM=\frac{A*10^3*10^6{L * N}](https://latex.codecogs.com/svg.latex?\Large&space;FPKM=\frac{A*10^3*10^6}{L * N})

Where *A* is the reads mapped (or aligned in the case of ResFinder) to a gene, *L* is the gene length in bp and *N* is the normalization count.
*N* could be bacterial abundance (used when calculating FPKM for ARG abundance), total number of reads from a given library or any other number that abundances need to be scaled to.

## How to use custom databases
Abundance tables for custom databases (i.e. not listed in the DB list for this tool - see Advanced options) can be produced by pointing argument -i to a directory containing the mapstat files and selecting parameter -cdb.

Due to no .refdata files being available for custom databases, only CLR, relativeAbundance and fragmentCount tables on accession ID level will be produced.

## Resfinder Class annotation

In order to aggregate ResFinder genes to a class level, a ResFinder.class file produced for the database is used.
The script will use a default file that may not be up to date with new ResFinder versions if no class annotation file is specified.

The ResFinder.class file can be found in the following location on C2:
/home/databases/metagenomics/db/ResFinder_VERSION/ResFinder.class

Where VERSION refers to the newest ResFinder database version (f.eks. ResFinder_20200125).

## Advanced Options

argument     | input           | message                                                  | info
------------ | --------------- | ---------------------------------------------------      | -------------
-h, --help   |                 | Shows help message                                       |
-i           | <MAPSTAT_DIR> | Path to directory or list containing .mapstat files     | *mandatory*
-o           | <OUTPUT_DIR>    | Where to put output directory                | *mandatory*
-db          | <DATABASE>   | Select which database was used for KMA mapping               | *mandatory except when running -cdb*
-cdb           | <CUSTOM_DB>     | Custom database. Select if input mapstat directory (-i) points to a directory containing mapstat files from a custom database                |
-tax         | <TAXONOMY>      | Which taxonomical level to create abundance tables for\*\*      | *mandatory for certain databases*
-ref         | <REFDATA_FILES> | Path to directory containing .refdata file      |
-cnf          | <COUNT_NORMALIZATION_FILE>| Path to the count normalization file  
-rf_class    | <RESFINDER_CLASS_FILE> | Path to ResFinder class file  |         |
-clr         | <>    | Select to produce CLR-transformed tables                 |
-fpkm        | <>    | For resistance gene databases: select to produce FPKM-normalized fragmentCountAln tables. |
-tpm         | <>    | For resistance gene databases: select to produce TPM-normalized fragmentCountAln tables. |
-top         | <n>             | Get abundance tables for top n most abundant features for each sample | 
-top_a       | <n>             | Get abundance tables for top n most abundant features across all samples |
-aln           | <FRAGMENTCOUNT_ALN>       | Select to force use of fragmentCountAln                       |
-ss           | <SUBSET>       | Silva database subset. Choose from: bacteria, eucaryota, archaea                       |
-l           | <LOGFILE>       | Put all text in logfile  |
-v           | <TRUE/FALSE>       | Select to activate verbose output                        |

\* Database choices are:
silva, resfinder, __bacteria__, __fungi__, __protozoa__, __parasite__, __archaea__, __human__, __imgvr__, __kvit__, virus, functionalresistance, metalresistance, mitochondrion, plastid, kvi.  
All databases marked with __bold__ are included in the genomic/genomic2 database. If mapping to genomic/genomic2, select one of the databases in bold to get abundance tables.

\*\* Taxonomy choices are:
accession, strain, species, genus, family, order, class, phylum, superkingdom, all