# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 17:09:33 2020

@author: Fredespade
"""
import os

def get_last_line(file):
    ''' Returns the last line of the input file '''
    
    # Open file for reading in binary mode
    with open(file, "rb") as fh:
        
        # Move the cursor to the end of the file
        fh.seek(0, os.SEEK_END)
        
        # Create a buffer to store the characters of the last line
        buffer = bytearray()
        
        # Get the position of pointer (currently EOF)
        pointer_location = fh.tell()
        
        # Loop until pointer reaches the beginning of file (it will return once it hits end of last line)
        while pointer_location >= 0:
            
            # Move the file pointer to the location pointed by pointer_location
            fh.seek(pointer_location)
            
            # Go one character backwards (-1)
            pointer_location = pointer_location -1
            
            # Read that byte / character
            new_byte = fh.read(1)
            
            # If the read byte is new line character then it means we have reached the end of the first line
            if new_byte == b'\n':
                # Return last line
                return(buffer.decode()[::-1])
            else:
                # If last read character is not \n then store the character in the buffer
                buffer.extend(new_byte)

    return(None)

class Mapstat():

    def __init__(self, file):
        ''' Creates a mapstat object'''
        
        # Fields for holding mapstat info
        self.file = file
        self.fname = os.path.basename(file)
        self.db_name = None
        self.database = None
        self.fragmentCount_sample = None
        self.header = None
        self.file_head = None
        self.kma_version = None
        self.features_found = None
        self.data_good = None
        self.samplename = None
        
        # Run diagnostics for mapstat file and get file_header (if possible)
        self.diagnostics(file)
        
        # If file is looking good, proceed with assigning fields
        if self.features_found and self.data_good:
            try:
                
                # Get samplename
                if "__" in self.fname:
                    self.samplename = self.fname.split("__")[1].split(".mapstat")[0]
                else:
                    self.samplename = self.fname.split(".mapstat")[0]
                    
                # Set basic info
                self.get_db_name()
                self.get_fragment_count_sample()
                self.get_kma_version()
            except:
                print("Error parsing data in mapstat file")
                raise


    def diagnostics(self, file):
        
        # First check if we have any features in file
        try:
            self.get_file_head()
            self.get_header()
            self.features_found = True
        except:
            self.features_found = False
        
        # Next check if we have missing information in the end of file (if we have found features)
        if self.features_found:
            lastline = get_last_line(file)
                
            # If the last line has fewer columns than the header, we are missing information
            if lastline != "" and len(lastline.split("\t")) < len(self.header):
                self.data_good = False
            else:
                self.data_good = True

        return()
            
    def get_file_head(self):
        ''' Tries to read the first 8 lines of mapstat file '''
        
        with open(self.file, "r") as myfile:
            head = [next(myfile) for line in range(8)]
        
        self.file_head = head
        
        return()
    
    def get_db_name(self):
        ''' Looks in the file_head to find database information '''
        
        ls = self.file_head[2].strip().split()
        
        self.database = ls[-1].lower()
        self.db_name = self.database.split("_")[0].lower()
        
        return()
    
    
    def get_fragment_count_sample(self):
        ''' Looks in the file_head to find total fragmentCount information '''
    
        self.fragmentCount_sample = self.file_head[3].strip().split()[-1]
        
        return()
    
    def get_header(self):
        ''' Looks in the file_head and sets the file header '''
        
        self.header = set(self.file_head[6].strip().split())
        
        return()
    
    def get_kma_version(self):
        ''' Looks in the file_head to find KMA version '''
        
        self.kma_version = self.file_head[1].strip().split()[-1]
        
        return()