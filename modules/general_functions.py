# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 10:33:44 2019

@author: Fredespade
"""

# Import libraries
import pandas as pd
import os
import sys
import csv

###############################################################################
############################### GENERAL FUNCTIONS #############################
###############################################################################

def estimate_workload(args, n_threshold = 100, mem_threshold = 250):
    '''
    This method checks the number of mapstat files, and calculates
    an approximate total size of the dataset that is being analyzed.

    If the size of the dataset is above certain thresholds, the user will be warned
    that the script will take a while to run, and that it may be necessary to 
    run on an interactive node on Computerome.
    '''
    # Basic stats
    num_files = len(args.mapstat_files)
    total_size = 0

    if num_files > n_threshold:
        for file in args.mapstat_files:
            filesize = os.path.getsize(file)
            total_size += (filesize / 1024 ** 2)

        if total_size > mem_threshold:
            print("Large data set (%s mapstat files, > 250 MB) detected. Execution will be slow, especially if producing tables at species level." % str(num_files), flush = True)
            print("If not doing so already, consider running on an interactive node on Computerome.\n", flush = True)

    return()


def merge_dataframes(df1, database, df2 = None, l_on = None, r_on = None, filetype = None, dest = None):
    ''' 
    Merges dataframes. 

    Input can be a list of dataframes (df1), or just two dataframes (df1 and df2).

    The DataFrame df1 will always be .mapstat file(s), while df2 can be other files
    such as .refdata or normalization count file

    When merging two DataFrames, it checks if any features are lost. This can happen
    if a .refdata or norm count file doesn't contain all features found in the 
    .mapstat file. 
    '''
    


    # Keep track of number of features (rows)
    if isinstance(df1,pd.DataFrame) and isinstance(df2,pd.DataFrame):
        rows_before = len(df1.index)

    try:
        # Input is a list of DataFrames
        if not isinstance(df1,pd.DataFrame):

            if len(df1) > 1:
                merged_df = pd.concat(df1)
            elif len(df1) == 1:
                merged_df = df1[0]
            else:
                merged_df = pd.DataFrame()

        # Input is two DataFrames
        else:
            
            merged_df = pd.merge(left = df1, right = df2, left_on = l_on, right_on = r_on)

            # Check if we lost features after merging, warn user
            rows_after = len(merged_df.index)
            
            # If we have lost rows, write features with missing taxonomy to file
            if rows_before > rows_after:
                if filetype == ".refdata":
                    handle_missing_tax(df1, merged_df, dest, filetype, rows_before, rows_after, database)
    except:
        print("Source of error: function merge_dataframes()")
        raise

    return(merged_df)

def handle_missing_tax(df1, merged_df, dest, filetype, rows_before, rows_after, database):
    
    # Make sets of features before and after
    features_before = set(df1["id"])
    features_after = set(merged_df["id"])
    
    # Find the difference in features
    missing_features = features_before - features_after
    
    # Many missing features, write them to file
    missing_features_df = df1[df1["id"].isin(list(missing_features))]
    missing_features_file = dest.replace("\\", "/") + "/" + str(database) + "_" + "sequence_ids_without_tax_annotation.txt"
    missing_features_df.to_csv(missing_features_file, sep = "\t", quoting=csv.QUOTE_NONE, index=False)
    
    print("##### WARNING #####\n")
    print("Missing taxonomical annotations for %s sequence ids in the %s %s file." % (str(len(missing_features)), database,filetype), flush = True)
    print("These sequence ids will not be included in abundance tables. A list of the sequence ids and their abundance can be found here: \n%s\n" % missing_features_file, flush = True)

    return()

def create_output_folder(base_output):

    # Keep track of folders we are creating, for cleaning purposes
    args_output_dirs = dict()
    args_created_dirs = dict()

    # Create main output folder
    if not os.path.isdir(base_output):
        try:
            os.mkdir(base_output)

            # Add mainout to folders_created
            args_output_dirs["mainout"] = base_output
            args_created_dirs["mainout"] = base_output

        except Exception:
            print("Unknown error trying to create main output folder.")
            raise
    else:
        args_output_dirs["mainout"] = base_output

    return(args_output_dirs, args_created_dirs)


def generate_output_dirs(args, base_output, data_container_obj):
    ''' 
    Creates output directories.
    '''

    # Add mainout to folders_created
    args.output_dirs["mainout"] = base_output
    args.created_dirs["mainout"] = base_output

    # Create database subfolders
    if len(args.db) > 0:
        for db in args.db :
    
            if db =="genomic" or db == "genomic2":
                if not args.do_genomic:
                    continue
            elif db == "genomic_overview":
                continue
            elif db == "kvi":
                if not args.do_kvi:
                    continue
            
            outdir = base_output + "/" + db
            if not os.path.isdir(outdir):
                try:
                    os.mkdir(outdir)
                    args.output_dirs[db] = outdir
                    args.created_dirs[db] = outdir
                except Exception:
                    print("Unknown error trying to create output folder for database %s." % db)
                    raise
            else:
                args.output_dirs[db] = outdir
                
    elif args.custom_db:
        
        outdir = base_output + "/custom_database"
        if not os.path.isdir(outdir):
            try:
                os.mkdir(outdir)
                args.output_dirs["custom"] = outdir
                args.created_dirs["custom"] = outdir
            except Exception:
                print("Unknown error trying to create output folder for custom database.")
                raise
        else:
            args.output_dirs["custom"] = outdir

        
    return(args.output_dirs, args.created_dirs)

def prettyprint(dataframe, l = 5):
    ''' 
    Pretty prints a Pandas DataFrame, showing all columns instead of standard 
    Panda's print format.
    '''

    # More options can be specified also
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(dataframe.head(l), flush = True)

    return()

def parse_normalization_measures(n_measure, args_db, data_container_obj):
    '''
    Calculating TPM or FPKM can only be performed for databases where 
    /reference sequence length is available, i.e. ResFinder and other
    resistance gene databases.
    
    In addition, they can only be calculated if we have a count against which to normalize,
    such as a total bacterial fragments mapped. This should be specified with the args.count_norm_file.
    
    This method checks if there are any missing data when either FPKM or TPM
    normalization is needed.
    '''
    
    # Check if we have .overview file for genomic database
    if not data_container_obj.has_norm:
        print("%s normalization requires normalization file argument -cnf / --count_normalization_file to be active." % n_measure, flush = True)
        print("No normalization count file found, skipping %s normalization." % n_measure, flush = True)
        return(False)
        
    # Check if we have .refdata files needed
    missing_refdata = list()
    if "resfinder" in args_db:
        if not data_container_obj.resfinder_has_ref:
            missing_refdata.append("resfinder")
    if "functionalresistance" in args_db:
        if not data_container_obj.functionalresistance_has_ref:
            missing_refdata.append("functionalresistance")
    if "metalresistance" in args_db:
        if not data_container_obj.metalresistance_has_ref:
            missing_refdata.append("metalresistance")
    if "mge" in args_db:
        if not data_container_obj.MGE_has_ref:
            missing_refdata.append("mge")
    if "silva" in args_db:
        if not data_container_obj.silva_has_ref:
            missing_refdata.append("silva")
        
    if len(missing_refdata) > 0:
        print("No corresponding .refdata files found for database(s) %s. Skipping %s normalization." % (str(missing_refdata), n_measure), flush = True)
        print("%s normalization requires gene lengths from the .refdata files." % n_measure, flush = True)
        return(False)
    
    return(True)

###############################################################################
############################## MEMORY MANAGEMENT ##############################
###############################################################################
    
def mem_usage(pandas_obj):
        ''' 
        Calculates memory usage of a pandas DataFrame or Series.
        '''

        # If doesn't exist
        if pandas_obj.empty:
            return(0)

        # Calculate memory usage
        try:
            if isinstance(pandas_obj,pd.DataFrame):
                usage_bytes = pandas_obj.memory_usage(deep = True).sum()
            # If not a dataframe, it should be series
            else:
                usage_bytes = pandas_obj.memory_usage(deep = True)
        except:
            raise ValueError("Error trying to calculate the memory usage of dataframe in function mem_usage().")

        # Convert bytes to megabytes
        usage_mb = usage_bytes / 1024 ** 2

        return(usage_mb)

def convert_to_category(pandas_dataframe):
    ''' 
    Reduces memory usage of a pandas DataFrame.

    Goes over each column, and detects if less than 50% of the values in the column
    are unique. If not, the values in the column are transformed into categories.

    Categorical data is stored as a key (hash) replacing the actual value, 
    meaning data such as strings are reduced significantly in memory.

    '''

    # Go over each column, count number of unique vs. total values
    for column in pandas_dataframe.columns:

        number_unique_values = len(pandas_dataframe[column].unique())
        number_total_values = len(pandas_dataframe[column])

        # If less than 50% of values are unique, do a conversion
        if number_unique_values / number_total_values < 0.50:
            if pandas_dataframe[column].dtype.name != 'category':
                pandas_dataframe[column] = pandas_dataframe[column].astype('category')

    return(pandas_dataframe)


# Print iterations progress
def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '#' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()
