# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 13:31:48 2019

@author: freddu
"""

# Import libraries
import pandas as pd
import csv
import sys
import modules.general_functions as general_functions
import numpy as np
import modules.pycoda as coda
from modules.general_functions import prettyprint as pp
import os

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Avoid Pandas printing warnings when slicing DataFrames
pd.options.mode.chained_assignment = None

###############################################################################
############################## CALCULATING STATS ##############################
###############################################################################


def run_mapstat_parser(args, data_container_obj):
    ''' 
    Runs the mapstat parser.

    For each database that is analyzed, submethods are called to analyse the specific database.
    '''
    
    ### PARSE GENOMIC DATABASES
        
    if args.do_kvi:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.kvi_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in kvI dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        process_genomic_databases(data_container_obj.kvi_df, args, "kvi", count_measure, data_container_obj)

    if "bacteria" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # Subset the genomic data that is bacterial
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.superkingdom_name == "Bacteria"]

        process_genomic_databases(subset_df, args, "bacteria", count_measure, data_container_obj)
        

    if "fungi" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'

        # Subset the genomic.mapstat data that comes from fungi
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("fungi", case = False)]
            
        process_genomic_databases(subset_df, args, "fungi", count_measure, data_container_obj)
        
    if "protozoa" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # Subset the genomic.mapstat data that comes from fungi
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("protozoa", case = False)]
            
        process_genomic_databases(subset_df, args, "protozoa", count_measure, data_container_obj)
        
    if "parasite" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # Subset the genomic.mapstat data that comes from fungi
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("parasite", case = False)]
            
        process_genomic_databases(subset_df, args, "parasite", count_measure, data_container_obj)
        
    if "archaea" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # Subset the genomic.mapstat data that comes from fungi
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.superkingdom_name == "Archaea"]
            
        process_genomic_databases(subset_df, args, "archaea", count_measure, data_container_obj)
    
    if "human" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.genomic_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # Subset the genomic.mapstat data that comes from fungi
        subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("human_", case = False)]
            
        process_genomic_databases(subset_df, args, "human", count_measure, data_container_obj)
        
    
    if "imgvr" in args.db:
        
        if args.kvi_available and data_container_obj.kvi_has_ref:
            
            if args.v:
                
                print("Extracting IMGVR data from kvI mapstat files...\n")
                
            # Check which measure to use
            if args.aln:
                if "fragmentCountAln" in data_container_obj.kvi_df:
                    count_measure = 'fragmentCountAln'
                else:
                    print("## WARNING: fragmentCountAln column not found in kvI dataframe. Proceeding with fragmentCounts.\n\n")
                    count_measure = 'fragmentCount'
            else:
                count_measure = 'fragmentCount'
            
            # Subset the genomic.mapstat data that comes from imgvr
            subset_df = data_container_obj.kvi_df[data_container_obj.kvi_df.db_name.str.contains("imgvr", case = False)]
            
            process_genomic_databases(subset_df, args, "imgvr", count_measure, data_container_obj)
        else:
            
            if args.v:
                print("Extracting IMGVR data from Genomic mapstat files...\n")
            
            if data_container_obj.genomic_has_ref:
                
                # Check which measure to use
                if args.aln:
                    if "fragmentCountAln" in data_container_obj.genomic_df:
                        count_measure = 'fragmentCountAln'
                    else:
                        print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                        count_measure = 'fragmentCount'
                else:
                    count_measure = 'fragmentCount'
                
                # Subset the genomic.mapstat data that comes from imgvr
                subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("imgvr", case = False)]
            
                process_genomic_databases(subset_df, args, "imgvr", count_measure, data_container_obj)
        
    if "kvit" in args.db:
        
        if args.kvi_available:
            
            if args.v:
                
                print("Extracting kvit data from kvI mapstat files...\n")
                
            # Check which measure to use
            if args.aln:
                if "fragmentCountAln" in data_container_obj.kvi_df:
                    count_measure = 'fragmentCountAln'
                else:
                    print("## WARNING: fragmentCountAln column not found in kvI dataframe. Proceeding with fragmentCounts.\n\n")
                    count_measure = 'fragmentCount'
            else:
                count_measure = 'fragmentCount'
            
            # Subset the genomic.mapstat data that comes from imgvr
            subset_df = data_container_obj.kvi_df[data_container_obj.kvi_df.db_name.str.contains("kvit", case = False)]

        else:
            
            if args.v:
                
                print("Extracting kvit data from Genomic mapstat files...\n")
                
            # Check which measure to use
            if args.aln:
                if "fragmentCountAln" in data_container_obj.genomic_df:
                    count_measure = 'fragmentCountAln'
                else:
                    print("## WARNING: fragmentCountAln column not found in Genomic dataframe. Proceeding with fragmentCounts.\n\n")
                    count_measure = 'fragmentCount'
            else:
                count_measure = 'fragmentCount'
        
            # Subset the genomic.mapstat data that comes from fungi
            subset_df = data_container_obj.genomic_df[data_container_obj.genomic_df.db_name.str.contains("kvit", case = False)]
            
        process_genomic_databases(subset_df, args, "kvit", count_measure, data_container_obj)
        
        
    if "virus" in args.db:
        
        if args.kvi_available:
            
            # Check which measure to use
            if args.aln:
                if "fragmentCountAln" in data_container_obj.kvi_df:
                    count_measure = 'fragmentCountAln'
                else:
                    print("## WARNING: fragmentCountAln column not found in kvI dataframe. Proceeding with fragmentCounts.\n\n")
                    count_measure = 'fragmentCount'
            else:
                count_measure = 'fragmentCount'
            
            # Subset the genomic.mapstat data that comes from imgvr
            subset_df = data_container_obj.kvi_df[data_container_obj.kvi_df.db_name.str.contains("virus", case = False)]

            process_genomic_databases(subset_df, args, "virus", count_measure, data_container_obj)
            
        else:
            print("Database Virus was not found, kvI mapstat files not present. Skipping...\n")
            

        ### Parse 16s/18s
    if "silva" in args.db:
        
        # Check if we need to select only certain superkingdom
        try:

            if args.subset_silva and data_container_obj.silva_has_ref:
                data_container_obj.silva_df = data_container_obj.silva_df[data_container_obj.silva_df.superkingdom_name == args.subset_silva]
                
            elif args.subset_silva and not data_container_obj.silva_has_ref:
                print("WARNING: tried to subset %s Silva sequences, but no .refadata file was available. Will continue without subsetting Silva.\n" % args.subset_silva)
        except:
            print("Error occurred while trying to select %s subset from Silva results.\n" % args.subset_silva)
    
        if data_container_obj.silva_df.empty:
            print("Error: after subsetting Silva %s sequences, no data was left to process. Skipping Silva database.\n" % args.subset_silva)
        else:
            # List of the features we should subset
            
            if args.aln:
                subset_list = ['fragmentCountAln', 'sample_fragmentCount','sample', 'id']
                count_measure = "fragmentCountAln"
            else:
                subset_list = ['fragmentCount', 'sample_fragmentCount','sample', 'id']
                count_measure = "fragmentCount"
                     
            # Add the features that are known and relevant                       
            if data_container_obj.silva_has_ref:
                subset_list.append('id_len')
            if args.taxonomy != None:
                for tax in args.taxonomy:
                    subset_list.append(tax)
               
            # Create subset
            df_subset = data_container_obj.silva_df[subset_list]
            
            # Process
            process_genomic_databases(df_subset, args, "silva", count_measure, data_container_obj)
        
        
    if "mitochondrion" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.mitochondrion_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Mitochondrion dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # List of the features we should subset
        subset_list = [count_measure, 'sample_fragmentCount','sample', 'id']
                 
        # Add the features that are known and relevant                       
        if data_container_obj.mitochondrion_has_ref:
            subset_list.append('id_len')
        if args.taxonomy != None:
            for tax in args.taxonomy:
                subset_list.append(tax)
           
        # Create subset
        df_subset = data_container_obj.mitochondrion_df[subset_list]
        
        # Process
        process_genomic_databases(df_subset, args, "mitochondrion", "fragmentCount", data_container_obj)
        
        
    if "plastid" in args.db:
        
        # Check which measure to use
        if args.aln:
            if "fragmentCountAln" in data_container_obj.plastid_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in Plastid dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        
        # List of the features we should subset
        subset_list = [count_measure, 'sample_fragmentCount','sample', 'id']
                 
        # Add the features that are known and relevant                       
        if data_container_obj.plastid_has_ref:
            subset_list.append('id_len')
        if args.taxonomy != None:
            for tax in args.taxonomy:
                subset_list.append(tax)
           
        # Create subset
        df_subset = data_container_obj.plastid_df[subset_list]
        
        # Process
        process_genomic_databases(df_subset, args, "plastid", "fragmentCount", data_container_obj)
        
    if "mge" in args.db:
        
        # List of the features we should subset
        if args.aln:
            if "fragmentCountAln" in data_container_obj.mge_df:
                count_measure = 'fragmentCountAln'
            else:
                print("## WARNING: fragmentCountAln column not found in MGE dataframe. Proceeding with fragmentCounts.\n\n")
                count_measure = 'fragmentCount'
        else:
            count_measure = 'fragmentCount'
        subset_list = ['sample', 'id', 'sample_fragmentCount', count_measure]
                 
        # Add the features that are known and relevant                       
        if data_container_obj.MGE_has_ref:
            subset_list.append('id_len')
           
        # Create subset
        df_subset = data_container_obj.mge_df[subset_list]
        
        # Process
        process_functional_databases(df_subset, args, data_container_obj, "mge", count_measure)
        
        
    ######### RESISTANCE DATABASES
    
    ### PARSE RESFINDER
        
    if "resfinder" in args.db:
        
        # List of the features we should subset
        subset_list = ['sample', 'id', 'class']
        
        # Add the features that are known and relevant
        if 'fragmentCountAln' in data_container_obj.resfinder_df:
            subset_list.append('fragmentCountAln')
            count_measure = 'fragmentCountAln'
        else:
            print("## WARNING: fragmentCountAln column not found in ResFinder dataframe. Proceeding with fragmentCounts.\n\n")
            subset_list.append('fragmentCount')
            count_measure = 'fragmentCount'
            
        if data_container_obj.resfinder_has_ref:
            subset_list.append("id_len")
            
        # Create subset 
        res_subset = data_container_obj.resfinder_df[subset_list]
            
        process_resistance_databases(res_subset, args, data_container_obj, "resfinder", count_measure)
        
    ### PARSE FUNCTIONALRESISTANCE
        
    if "functionalresistance" in args.db:
        
        # List of the features we should subset
        subset_list = ['sample', 'id', 'class']
        
        # Add the features that are known and relevant
        if 'fragmentCountAln' in data_container_obj.functional_df:
            subset_list.append('fragmentCountAln')
            count_measure = 'fragmentCountAln'
        else:
            print("## WARNING: fragmentCountAln column not found in ResFinder dataframe. Proceeding with fragmentCounts.\n\n")
            subset_list.append('fragmentCount')
            count_measure = 'fragmentCount'

        if data_container_obj.functionalresistance_has_ref:
            subset_list.append("id_len")
            
        # Create subset 
        res_subset = data_container_obj.functional_df[subset_list]
                
        # Process
        process_resistance_databases(res_subset, args, data_container_obj, "functionalresistance", count_measure)
        
        
    if "metalresistance" in args.db:
        
        # List of the features we should subset
        subset_list = ['sample', 'id', 'class']
        
        # Add the features that are known and relevant
        if 'fragmentCountAln' in data_container_obj.metal_df:
            subset_list.append('fragmentCountAln')
            count_measure = 'fragmentCountAln'
        else:
            print("## WARNING: fragmentCountAln column not found in ResFinder dataframe. Proceeding with fragmentCounts.\n\n")
            subset_list.append('fragmentCount')
            count_measure = 'fragmentCount'
            
        if data_container_obj.metalresistance_has_ref:
            subset_list.append("id_len")
            
        # Create subset 
        res_subset = data_container_obj.metal_df[subset_list]
                
        # Process
        process_resistance_databases(res_subset, args, data_container_obj, "metalresistance", count_measure)
        
        
    # Check if any samples were dropped from TPM/FPKM normalization due to missing info about bacterial loads in sample
    if len(set(data_container_obj.cnf_equal_zero)) > 0:
        print("WARNING: norm count in the %s file reported to be 0 (zero) for: %s" % (os.path.basename(data_container_obj.norm_file), set(data_container_obj.cnf_equal_zero)), flush = True)
        print("These were dropped in the TPM/FPKM normalized abundance tables.\n")
            
    if args.custom_db:
        
        # List of the features we should subset
        subset_list = ['sample', 'id', 'sample_fragmentCount']

        # Create subset
        fc_aln = False
        if args.aln:
            if 'fragmentCountAln' in data_container_obj.custom_df:
                subset_list.append('fragmentCountAln')
                count_measure = 'fragmentCountAln'
                fc_aln = True
            else:
                print("## WARNING: fragmentCountAln column not found in Custom DB dataframe. Proceeding with fragmentCounts.\n\n")
                subset_list.append('fragmentCount')
                count_measure = 'fragmentCount'
        else:
            subset_list.append('fragmentCount')
            count_measure = 'fragmentCount'
                    
        # Create subset
        try:
            df_subset = data_container_obj.custom_df[subset_list]
        except:
            print("Error: occurred while trying to subset custom database table.", flush = True)
            raise
        
        # Process
        process_custom_database(df_subset, args, "custom", data_container_obj, fc_aln, count_measure)


    return()

def process_custom_database(df_subset, args, database, data_container_obj, fc_aln, count_measure):
    
    # Make file timestamp
    if args.v:
        print("# Processing custom database, using %ss..." % count_measure, flush = True)

    # Check if we have any empty values:
    empty_df = df_subset.loc[df_subset[count_measure] == ""]
    if len(empty_df.index) > 1:
        raise ValueError("Error while trying to parse data. Empty %s values found in mapstat files." % count_measure)
            
    # Select tax
    tax = "id"
    
    # Create fragmentCount matrix and print to file
    fc_mat = get_abundance_table(df_subset, \
                                     count_measure = count_measure, \
                                     subset = [count_measure,'sample', tax], \
                                     groupby = ['sample', tax], \
                                     norm_measure = count_measure, database = database)
    write_to_file(args, fc_mat, database, str(database) + "_" + str(tax) + "_" + count_measure + ".txt", "\t", '%.f', "fragmentCount")
    
    
    # Create relabundance matrix and print to file
    relab_mat = get_abundance_table(df_subset, \
                                     count_measure = count_measure, \
                                     subset = ['sample', tax], \
                                     groupby = ['sample', tax], \
                                     norm_measure = 'relabundance', database = database)
    write_to_file(args, relab_mat, database, str(database) + "_" +  str(tax) + "_" + str(count_measure) + "_relabundance.txt", "\t", '%.5f', "relAbundance")

    
    # Create CLR matrix and print to fil
    if args.clr:
        
        if args.v:
            print("\n\nProducing %s %s CLR at %s level..." % (database, count_measure, tax), flush = True)

        clr_matrix = coda.clr(fc_mat)
        write_to_file(args, clr_matrix, database, str(database) + "_" + str(tax) + "_" + str(count_measure) + "_CLR.txt", "\t", '%.5f', "CLR")

    # Get top across all samples and write to file
    if args.top_a:
        
        if args.v:
            print("\n\nProducing %s %s top -a %s at %s level..." % (database, count_measure, args.top_a, tax), flush = True)
        top_x_across = get_top_all(relab_mat, args, 'fragmentCount')
        write_to_file(args, top_x_across, database, str(database) + "_" + str(tax) + "_fragmentCount_top_" + str(args.top_a) + "_across.txt", "\t", '%.5f', "top 10 across")

    # Get top for each sample
    if args.top:
        
        if args.v:
            print("\n\nProducing %s %s top %s at %s level..." % (database, count_measure, args.top, tax), flush = True)
        top_x = get_top(relab_mat, args, 'fragmentCount', database)
        if not top_x.empty:
            write_to_file(args, top_x, database, str(database) + "_" + str(tax) + "_fragmentCount_top_" + str(args.top) + ".txt", "\t", '%.5f', "top 10")
    
        
    if args.v:
        print()
        
    if fc_aln:
        
        # Now we try to create fragmentCountAln tables
        count_measure = "fragmentCountAln"
        
        # Check if we have any empty values:
        empty_df = df_subset.loc[df_subset[count_measure] == ""]
        
        if len(empty_df.index) > 1:
            raise ValueError("Error while trying to parse data. Empty %s values found in mapstat files." % count_measure)
                
        # Select tax
        tax = "id"
        
        # Create fragmentCount matrix and print to file
        fc_mat = get_abundance_table(df_subset, \
                                         count_measure = count_measure, \
                                         subset = [count_measure,'sample', tax], \
                                         groupby = ['sample', tax], \
                                         norm_measure = count_measure, database = database)
        write_to_file(args, fc_mat, database, str(database) + "_" + str(tax) + "_" + count_measure + ".txt", "\t", '%.f', "fragmentCountAln")
        
        
        # Create relabundance matrix and print to file
        relab_mat = get_abundance_table(df_subset, \
                                         count_measure = count_measure, \
                                         subset = ['sample', tax], \
                                         groupby = ['sample', tax], \
                                         norm_measure = 'relabundance', database = database)
        write_to_file(args, relab_mat, database, str(database) + "_" +  str(tax) + "_" + str(count_measure) + "_relabundance.txt", "\t", '%.5f', "relAbundance")
    
        
        # Create CLR matrix and print to file
        
        if args.clr:
            
            if args.v:
                print("\n\nProducing %s %s CLR at %s level..." % (database, count_measure, tax), flush = True)
    
            clr_matrix = coda.clr(fc_mat)
            write_to_file(args, clr_matrix, database, str(database) + "_" + str(tax) + str(count_measure) + "_CLR.txt", "\t", '%.5f', "CLR")
    
        # Get top across all samples and write to file
        if args.top_a:
            
            if args.v:
                print("\n\nProducing %s %s top -a %s at %s level..." % (database, count_measure, args.top_a, tax), flush = True)
            top_x_across = get_top_all(relab_mat, args, 'fragmentCountAln')
            write_to_file(args, top_x_across, database, str(database) + "_" + str(tax) + str(count_measure) + "_top_" + str(args.top_a) + "_across.txt", "\t", '%.5f', "top 10 across")
    
        # Get top for each sample
        if args.top:
            
            if args.v:
                print("\n\nProducing %s %s top %s at %s level..." % (database, count_measure, args.top, tax), flush = True)
            top_x = get_top(relab_mat, args, 'fragmentCountAln', database)
            if not top_x.empty:
                write_to_file(args, top_x, database, str(database) + "_" + str(tax) + str(count_measure) + "_top_" + str(args.top) + ".txt", "\t", '%.5f', "top 10")
    
    return()


def process_genomic_databases(df_subset, args, database, count_measure, data_container_obj):

    if args.v:
        print("# Processing database %s..." % database, flush = True)
        
    # Remove duplicate columns
    df_subset = df_subset.loc[:,~df_subset.columns.duplicated()]
    
    # Check if we have any empty values:
    empty_df = df_subset.loc[df_subset[count_measure] == ""]
    if len(empty_df.index) > 1:
        raise ValueError("Error while trying to parse data. Empty %s values found in mapstat files." % count_measure)

    # Make table for each taxonomic level selecte
    if args.taxonomy == None:
        args.taxonomy = ["id"]
        
    # Check tax for database
    possible_tax_levels = set(args.taxonomy).intersection(set(data_container_obj.tax_levels[database]))
    
    for x in args.taxonomy:
        if x not in possible_tax_levels:
            print("Tax aggregation at %s level is not supported by database %s. Skipping." % (x.split("_")[0],database))
    
    if len(possible_tax_levels) == 0:
        possible_tax_levels = ["id"]

    for tax in possible_tax_levels:
             
        if args.v:
            print("\n\nProducing %s %s at %s level..." % (database, count_measure, tax), flush = True)
            
        # Get tax without "_name" at the end:
        if "_" in tax:
            taxn = tax.split("_")[0]
        else:
            taxn = tax
            
        # Create fragmentCount matrix and print to file
        fc_mat = get_abundance_table(df_subset, \
                                         count_measure = count_measure, \
                                         subset = [count_measure,'sample', tax], \
                                         groupby = ['sample', tax], \
                                         norm_measure = count_measure, database = database)
        write_to_file(args, fc_mat, database, str(database) + "_" + str(taxn) + "_" + count_measure + ".txt", "\t", '%.f', "fragmentCount")
        
        
        # Create relabundance matrix and print to file
        relab_mat = get_abundance_table(df_subset, \
                                         count_measure = count_measure, \
                                         subset = ['sample', tax], \
                                         groupby = ['sample', tax], \
                                         norm_measure = 'relabundance', database = database)
        write_to_file(args, relab_mat, database, str(database) + "_" +  str(taxn) + "_" + "relabundance.txt", "\t", '%.5f', "relAbundance")
    
        # Create CLR matrix and print to file
        
        if args.clr:
            
            if args.v:
                print("\nProducing %s CLR at %s level..." % (database, tax), flush = True)

            clr_matrix = coda.clr(fc_mat)
            write_to_file(args, clr_matrix, database, str(database) + "_" + str(taxn) + "_CLR.txt", "\t", '%.5f', "CLR")
    
        # Get top across all samples and write to file
        if args.top_a:
            
            if args.topm:
                if args.topm == "relativeAbundance":
                    ab_mat = relab_mat
                    dform = "%.5f"
                elif args.topm == "fragmentCount":
                    ab_mat = fc_mat
                    dform = "%f"
            else:
                ab_mat = relab_mat
                args.topm = "relativeAbundance"
                dform = "%.5f"
            
            if args.v:
                print("\nProducing %s top -a %s at %s level..." % (database, args.top_a, tax), flush = True)
            top_x_across = get_top_all(ab_mat, args, args.topm)
            write_to_file(args, top_x_across, database, str(database) + "_" + str(taxn) + "_" + str(args.topm) + "_top_" + str(args.top_a) + "_across.txt", "\t", dform, "top 10 across")
    
        # Get top for each sample
        if args.top:
            
            if args.topm:
                if args.topm == "relativeAbundance":
                    ab_mat = relab_mat
                    dform = "%.5f"
                elif args.topm == "fragmentCount":
                    ab_mat = fc_mat
                    dform = "%f"
                
            else:
                ab_mat = relab_mat
                args.topm = "relativeAbundance"
                dform = "%.5f"
            
            if args.v:
                print("\nProducing %s top %s at %s level..." % (database, args.top, tax), flush = True)
            top_x = get_top(ab_mat, args, args.topm, database)
            if not top_x.empty:
                write_to_file(args, top_x, database, str(database) + "_" + str(taxn) + "_" + str(args.topm) + "_top_" + str(args.top) + ".txt", "\t", dform, "top 10")
        
    if args.v:
        print()    
    
    return()

def process_resistance_databases(res_subset, args, data_container_obj, database, count_measure):
    
    if args.v:
        print("# Processing database %s..." % database, flush = True)
    
    # Check if we have any empty values:
    empty_df = res_subset.loc[res_subset[count_measure] == ""]
    if len(empty_df.index) > 1:
        raise ValueError("Error while trying to parse data. Empty %s values found in mapstat files." % count_measure)
        
    if args.v:
        print("\n\nProducing %s %s at gene and class level..."%(database, count_measure), flush = True)
        
    # Are we working with predicted phenotypes or predicted ARD families
    if database.lower() == "functionalresistance":
        meta_level = "_ARD_family_"
    else:
        meta_level = "_class_"

    # Get fragmenCount abundance tables, write to file
    res_gene_mat = get_abundance_table(res_subset, \
                                     count_measure = count_measure, \
                                     subset = False, \
                                     groupby = ['sample', 'id'], \
                                     norm_measure = count_measure, database = database)
    write_to_file(args, res_gene_mat, database, str(database) + "_genes_" + count_measure + ".txt", "\t", '%.f', count_measure)

    res_class_mat = get_abundance_table(res_subset, \
                                     count_measure = count_measure, \
                                     subset = False, \
                                     groupby = ['sample', 'class'], \
                                     norm_measure = count_measure, database = database)
    write_to_file(args, res_class_mat, database, str(database) + meta_level + count_measure + ".txt", "\t", '%.f', count_measure)
    
    # Get CLR abundance tables
    if args.clr:
        
        if args.v:
            print("\nProducing %s CLR at gene and tax level..." % (database), flush = True)

        try:
            # For genes
            clr_matrix = coda.clr(res_gene_mat)
            write_to_file(args, clr_matrix, database, str(database) + "_genes_CLR.txt", "\t", '%.5f', "CLR")

            # For classes
            clr_matrix = coda.clr(res_class_mat)
            write_to_file(args, clr_matrix, database, str(database) + meta_level + "CLR.txt", "\t", '%.5f', "CLR")
        except:

            # Print statement
            print("CLR values could not be produced. Make sure the pyCoDa package is correctly installed.")
            
    if database == "metalresistance" and any(arg for arg in [args.tpm, args.fpkm, args.top, args.top_a]):
        print("FPKM/TPM and -top arguments currently not supported for database metalresistance due to missing gene lengths. Skipping...\n")
        return()
        
    # Get TPM abundance tables
    res_gene_tpm = pd.DataFrame()
    if args.tpm:
        
        if args.v:
            print("\nProducing %s TPM at gene and tax level..." % (database), flush = True)
        
        n_measure = "TPM"
        res_gene_tpm = get_abundance_table(res_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'id'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_gene_tpm, database, str(database) + "_genes_" + n_measure + ".txt", "\t", '%.5f', n_measure)

        res_class_tpm = get_abundance_table(res_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'class'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_class_tpm, database, str(database) + meta_level + n_measure + ".txt", "\t", '%.5f', n_measure)

    # Get FPKM abundance tables
    res_gene_fpkm = pd.DataFrame()
    if args.fpkm:
        
        if args.v:
            print("\nProducing %s FPKM at gene and tax level..." % (database), flush = True)
        
        n_measure = "FPKM"
        res_gene_fpkm = get_abundance_table(res_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'id'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_gene_fpkm, database, str(database) + "_genes_" + n_measure + ".txt", "\t", '%.5f', n_measure)

        res_class_fpkm = get_abundance_table(res_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'class'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_class_fpkm, database, str(database) + meta_level + n_measure + ".txt", "\t", '%.5f', n_measure)
        
    normalized_dataframes = [res_gene_tpm, res_gene_fpkm]
    
    # Get top across all samples
    if args.top_a:
        
        if args.v:
            print("Producing across-sample top %s abundance table at gene and class level for resistance database %s..." % (args.top_a, database), flush = True)
        
        if not any([args.fpkm, args.tpm]):
            print("Could not produce top_a %s abundance table for resistance database %s: FPKM or TPM values must be selected." % (args.top_a, database))
        else:
            if args.fpkm:
                n_measure = "FPKM"
                df_idx = 1
            else:
                n_measure = "TPM"
                df_idx = 0
            
            top_x_across = get_top_all(normalized_dataframes[df_idx], args, n_measure)
            
            # Write to output
            filename = database + "_genes_" + n_measure + "_top_" + str(args.top_a) + "_across.txt"
            write_to_file(args, top_x_across, database, filename, "\t", '%.5f', n_measure)
    
            top_x_across = get_top_all(normalized_dataframes[df_idx], args, n_measure)
    
            # Write to output
            filename = database + meta_level + n_measure + "_top_" + str(args.top_a) + "_across.txt"
            write_to_file(args, top_x_across, database, filename, "\t", '%.5f', n_measure)
            
    # Get top for each sample
    if args.top:
        
        if args.v:
            print("Producing sample top %s abundance table at gene and class level for resistance database %s..." % (args.top, database), flush = True)
        
        if not any([args.fpkm, args.tpm]):
            	print("Could not produce top_a %s abundance table for resistance database %s: FPKM or TPM values must be selected." % (args.top_a, database))
        else:
            if args.fpkm:
                n_measure = "FPKM"
                df_idx = 1
            else:
                n_measure = "TPM"
                df_idx = 0
            
            top_x = get_top(normalized_dataframes[df_idx], args, n_measure, database)
            
            if not top_x.empty:
                # Write to output
                filename = database + "_genes_" + n_measure + "_top_" + str(args.top) + ".txt"
                write_to_file(args, top_x, database, filename, "\t", '%.5f', n_measure)
    
            top_x = get_top(normalized_dataframes[df_idx], args, n_measure, database)
            
            if not top_x.empty:
                # Write to output
                filename = database + meta_level + n_measure + "_top_" + str(args.top) + ".txt"
                write_to_file(args, top_x, database, filename, "\t", '%.5f', n_measure)

    if args.v:
        print()

    return()

def process_functional_databases(func_subset, args, data_container_obj, database, count_measure):
    
    if args.v:
        print("# Processing database %s..." % database, flush = True)
    
    # Check if we have any empty values:
    empty_df = func_subset.loc[func_subset[count_measure] == ""]
    if len(empty_df.index) > 1:
        raise ValueError("Error while trying to parse data. Empty %s values found in mapstat files." % count_measure)
        
    if args.v:
        print("\n\nProducing %s %s at gene level..."%(database, count_measure), flush = True)


    # Get fragmenCount abundance tables, write to file
    res_gene_mat = get_abundance_table(func_subset, \
                                     count_measure = count_measure, \
                                     subset = False, \
                                     groupby = ['sample', 'id'], \
                                     norm_measure = count_measure, database = database)
    write_to_file(args, res_gene_mat, database, str(database) + "_genes_" + count_measure + ".txt", "\t", '%.f', count_measure)
    
    # Create relabundance matrix and print to file
    relab_gene_mat = get_abundance_table(func_subset, \
                                     count_measure = count_measure, \
                                     subset = False, \
                                     groupby = ['sample', 'id'], \
                                     norm_measure = 'relabundance', database = database)
    write_to_file(args, relab_gene_mat, database, str(database) + "_" +  "_genes_" + "relabundance.txt", "\t", '%.5f', "relAbundance")
    
    # Get CLR abundance tables
    if args.clr:
        
        if args.v:
            print("\nProducing %s CLR at gene and tax level..." % (database), flush = True)

        try:
            # For genes
            clr_matrix = coda.clr(res_gene_mat)
            write_to_file(args, clr_matrix, database, str(database) + "_genes_CLR.txt", "\t", '%.5f', "CLR")

        except:

            # Print statement
            print("CLR values could not be produced. Make sure the pyCoDa package is correctly installed.")
            
        
    # Get TPM abundance tables
    res_gene_tpm = pd.DataFrame()
    if args.tpm:
        
        if args.v:
            print("\nProducing %s TPM at gene level..." % (database), flush = True)
        
        n_measure = "TPM"
        res_gene_tpm = get_abundance_table(func_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'id'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_gene_tpm, database, str(database) + "_genes_" + n_measure + ".txt", "\t", '%.5f', n_measure)

    # Get FPKM abundance tables
    res_gene_fpkm = pd.DataFrame()
    if args.fpkm:
        
        if args.v:
            print("\nProducing %s FPKM at gene level..." % (database), flush = True)
        
        n_measure = "FPKM"
        res_gene_fpkm = get_abundance_table(func_subset, \
                                         count_measure = count_measure, \
                                         subset = False, \
                                         groupby = ['sample', 'id'], \
                                         norm_measure = n_measure, database = database,
                                         data_container_obj = data_container_obj)
        write_to_file(args, res_gene_fpkm, database, str(database) + "_genes_" + n_measure + ".txt", "\t", '%.5f', n_measure)
        
    normalized_dataframes = [res_gene_tpm, res_gene_fpkm]

    return()

def get_abundance_table(input_dataframe, count_measure, subset, groupby, norm_measure, database, data_container_obj = None):
    '''
    Creates a fragmentcount matrix. Takes a pandas dataframe, finds abundances
    at a taxonomical level, and returns a fragmentcount abundance table.

    Parameters
    ----------
    args : argparse object
        Contains all arguments the program was run with.
    dataframe : pandas DataFrame()
        Dataframe containing all mapstat datafor a given KMA database.

    Returns
    -------
    fragmentcount_mat : pandas DataFrame()
        Dataframe containing a fragmentcount matrix.

    '''
    # Check number of samples before 
    n_samples_before = len(set(input_dataframe["sample"]))
    
    # Subset the matrix
    if subset == False:
        subset_df = input_dataframe
    else:
        subset_df = input_dataframe[subset]

    # Check if we need to calculate the measure specified
    if norm_measure == "relabundance":
        subset_df['relabundance'] = (input_dataframe[count_measure].astype('uint32') \
                                     / input_dataframe['sample_fragmentCount'].astype('uint32')) * 100
    elif norm_measure == "FPKM":
        subset_df = calculate_fpkm_tpm(subset_df, count_measure, data_container_obj, norm_measure, database, groupby[-1])
    elif norm_measure == "TPM":
        subset_df = calculate_fpkm_tpm(subset_df, count_measure, data_container_obj, norm_measure, database, groupby[-1])
        
    # Check we didn't get empty result
    if subset_df.empty:
        return(pd.DataFrame())
        
    # Convert to numeric format
    if norm_measure.lower() in ["fpkm", "tpm", "relabundance"]:
        subset_df[norm_measure] = subset_df[norm_measure].astype('float')
    else:
        subset_df[norm_measure] = subset_df[norm_measure].astype('uint32')

    #subset_df = dd.from_pandas(subset_df, chunksize=100000)
    abundance_matrix = subset_df.groupby(groupby, observed = True)[norm_measure].sum().fillna(value=0)
    
    # Unstack the long dataframe (make into wide format)
    abundance_matrix = abundance_matrix.unstack().fillna(value=0)

    # Remove features with a total count of 0 across all samples
    abundance_matrix = abundance_matrix.loc[:,(abundance_matrix.sum(axis=0) != 0)]
    
    # Check number of samples after 
    n_samples_after = len(abundance_matrix)
    
    # Check if number of samples is different
    if n_samples_after != n_samples_before and norm_measure.lower() not in ["fpkm","tpm"]:
        print("WARNING: %s samples lost when creating abundance table." % str(n_samples_before - n_samples_after), flush = True)
      
    return(abundance_matrix)


def calculate_fpkm_tpm(input_df, count_measure, data_container_obj, mtype, database, tax):
    
    # Get total number of fragments mapping to bacteria
    norm_count_df = data_container_obj.norm_dataframe
    norm_count_df.drop_duplicates(subset = "sampleId", inplace = True)
    
    # Find if any samples has a norm count of 0 (we cannot do FPKM or TPM for this sample)
    drop_samples = norm_count_df[norm_count_df['norm_count'] <= 0]

    # Remove samples with bacteria_fragmentCount_total from df
    norm_count_df = norm_count_df[~norm_count_df.sampleId.isin(drop_samples.sampleId)]
    
    # Add dropped samples to data_container obj so we can warn user
    data_container_obj.cnf_equal_zero.update(drop_samples.sampleId)
    
    # Now check if there are any samples in input df missing from norm_file
    missing_norm_count = list()
    input_df_samples = list(set(input_df["sample"].tolist()))
    norm_count_samples = list(set(norm_count_df["sampleId"].tolist()))
    for sample in input_df_samples:
        if sample not in norm_count_samples:
            missing_norm_count.append(sample)
          
    # Keep track of number of samples
    num_samples_pre = len(set(input_df['sample']))
            
    # Remove samples with missing norm count
    input_df = input_df[~input_df['sample'].isin(missing_norm_count)]
    if len(input_df['sample']) == 0:
        print("\n### Error: all samples found in input mapstat files were missing from the normalization count fine: %s\n" % data_container_obj.norm_file, flush = True)
        return(pd.DataFrame())

    # Calculate FPKM
    if mtype == "FPKM":
        
        # Get information about total bacterial abundances in fragmentCounts per sample into DF 
        df = pd.merge(input_df, norm_count_df, left_on = 'sample', right_on = 'sampleId')
        
        # Get permillion scaling factor by dividing total bacterial fragments with 1,000,000
        df['fcount_permillion'] = df['norm_count'] / 1000000

        # Divide fragmentcounts by "permillion" scaling factor, normalizing for depth,
        # giving reads per million (RPM)
        df['rpm'] = df[count_measure] / df['fcount_permillion']
        
        # Divide RPM values by length of gene (in kilobases) to get RPKM
        df['id_len_kb'] = df['id_len'].astype("uint32") / 1000
        df['FPKM'] = df['rpm'] / df['id_len_kb']
    
    # Calculate TPM
    elif mtype == "TPM":
        
        # Get information about total bacterial fragments per sample into DF
        df = pd.merge(input_df, norm_count_df, left_on = 'sample', right_on = 'sampleId')
        
        # Divide the fragment counts by length of each gene in kilobases. 
        # Gives readcounts per kilobase (RPK)
        df['id_len_kb'] = df['id_len'].astype("uint32") / 1000
        df['rpk'] = df[count_measure] / df['id_len_kb']

        # Sum up all RPK values for each sample and divide by 1,000,000. This gives
        # the permillion scaling factor
        rpk_sum = pd.DataFrame()
        rpk_sum['rpk_sum'] = df.groupby(['sample'], observed = True)['rpk'].sum()
        rpk_sum['rpk_permillion'] = rpk_sum['rpk_sum'] / 1000000
        
        # Divide the RPK values with the permillion scaling factor. This gives TPM.
        df = df.merge(rpk_sum, on = "sample")
        df['TPM'] = df['rpk'] / df['rpk_permillion']
        
    # Check if we have lost samples
    num_samples_post = len(set(df['sample']))
    missing_samples = num_samples_pre - num_samples_post
    if tax == "id":
        tax = "sequence id level"
    elif tax == "class":
        tax = "resistance class level"
    else:
        tax = tax + " tax level"
    if missing_samples > 0:
        print("WARNING: %s samples lost doing %s normalization on database %s on %s. Check if missing in normalization file, or if norm counts = 0." % (str(missing_samples), mtype, database, tax))

    return(df)


def get_top_all(df, args, datatype):
    ''' 
    Finds the top -n most abundant features in the dataframe across all samples.

    Does so by calculating the mean relative abundance of each  across all samples,
    and taking the top -n features with the highest mean abundance.
    
    '''
    # Top number
    n = args.top_a

    # Get the top n across all samples
    top_x_across = df.reindex(df.mean().sort_values().index, axis=1).iloc[:, -n:]
    
    # Reverse order of columns (to get most abundant listed first)
    top_x_across = top_x_across.iloc[:, ::-1]

    return(top_x_across)

def get_top(df, args, datatype, database):
    '''
    Finds the top -n most abundant features in the dataframe for each sample.

    Does so by obtaining the names of the top n features for each sample, and
    taking the values for each feature for each sample.
    
    As such the final number of features being returned may be higher (in theory
    up to m samples * top n features)
    
    '''
    # Make indexes for top -n
    n = args.top
    if n > len(df.index):
        n = len(df.index) - 1
    c = [str(i+1) for i in range(n)]

    # Get the names of the top -n features for each row (i.e. each sample)
    # Make a new matrix where each row is a sample, and columns go from 1 to n (args.top) of most abundant feature
    try:
        top_x_feature_names = (df.apply(lambda x: pd.Series(x.nlargest(n).index, index=c), axis=1).reset_index())
    except:
        #print("Could not produce top %s of the following: %s %s" %(args.top, datatype, database))
        return(pd.DataFrame())
    
    try:
        # Get all simple list consisting only of unique top most abundant features
        unique_features = pd.unique(top_x_feature_names[c].values.ravel("K"))
        
        # Now select these unique values from original dataframe
        top_x = df[unique_features]
        return(top_x)
    except:
        #print("Could not produce top %s of the following: %s %s" %(args.top, datatype, database))
        return(pd.DataFrame())

    return(pd.DataFrame())


def write_to_file(args, input_dataframe, database, filename, separator, f_format, datatype):

    # Check data is OK
    if input_dataframe.empty:
        print("Error trying to write %s %s data to file: %s. " % (database, datatype, filename))
        print("The DataFrame was empty.")
        return()

    # Print to output
    input_dataframe.to_csv(args.output_dirs[database] + "/" + filename, sep = separator, quoting=csv.QUOTE_NONE,  float_format=f_format)
    return()
