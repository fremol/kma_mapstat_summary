# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 13:44:25 2020

@author: freddu
"""

import pandas as pd

import modules.general_functions as general_functions

###############################################################################
############################ DATA_CONTAINER CLASS #############################
###############################################################################



class Data_container():
    ''' 
    Data_container class.

    The data_container object is made with the intent to track relevant data 
    in one centralised place, to make it easy to access in all parts of the 
    script. 

    Its main purpose is to track metadata related to .mapstat, .refdata, and norm files 
    files, but also has a few functions, mainly related to reduce the 
    amount of memory that is required when working with large datasets 
    (i.e. many large .mapstat files).
    '''

    def __init__(self):
        ''' 
        Create Data_container object.

        '''
        ######## DATABASE DATAFRAMES
        # Create dataframes for different databases
        self.genomic_df = pd.DataFrame()
        self.resfinder_df = pd.DataFrame()
        self.functional_df = pd.DataFrame()
        self.metalresistance_df = pd.DataFrame()
        self.silva_df = pd.DataFrame()
        self.kvi_df = pd.DataFrame()
        self.mitochondrion_df = pd.DataFrame()
        self.plastid_df = pd.DataFrame()
        self.MGE_df = pd.DataFrame()
        self.GMGC_df = pd.DataFrame()
        self.GS_df = pd.DataFrame()
        
        # For custom databases
        self.custom_df = pd.DataFrame()
        
        ######## REFDATA FILE TRACKING
        # Booleans to track which databases we have Refdata files for
        self.resfinder_has_ref = False
        self.genomic_has_ref = False
        self.functionalresistance_has_ref = False
        self.metalresistance_has_ref = False
        self.silva_has_ref = False
        self.kvi_has_ref = False
        self.mitochondrion_has_ref = False
        self.plastid_has_ref = False
        self.MGE_has_ref = False
        self.GMGC_has_ref = False
        self.GS_has_ref = False
        
        
        ######## NORMALIZATION COUNT FILE DATA
        # Booleans to track if we are able to do count normalization
        self.has_norm = False
        self.norm_file = None
        
        # Norm count dataframe - contains the counts used for raw fragmentCount scaling/normalization
        self.norm_dataframe = None

        # Ignore files that are in the count_normalization file, but not in the .mapstat files
        self.cnf_samples_not_found = set()
        
        # Keep track of samples that had a norm count equal to 0 (zero) in the args.count_norm_file
        self.cnf_equal_zero = set()
        
    
        ######## DATABASE TAXONOMY INFORMATION TRACKING
        # Keep track of which taxonomical information is available for each database.
        all_tax = ["superkingdom_name", "phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "# id", "id"]
        self.tax_levels = dict()
        
        ## Genomic
        self.tax_levels["genomic"] = all_tax
        self.tax_levels["bacteria"] = all_tax
        self.tax_levels["fungi"] = all_tax
        self.tax_levels["protozoa"] = all_tax
        self.tax_levels["parasite"] = all_tax
        self.tax_levels["archaea"] = all_tax
        self.tax_levels["human"] = all_tax
        
        ## kvI
        self.tax_levels["kvi"] = all_tax
        self.tax_levels["imgvr"] = all_tax
        self.tax_levels["virus"] = all_tax
        self.tax_levels["kvit"] = all_tax
        
        # 16s
        self.tax_levels["silva"] = all_tax
        
        # Mitochondrion
        self.tax_levels["mitochondrion"] = all_tax
        
        # Plastid
        self.tax_levels["plastid"] = ["id"]
        
        # MGE
        self.tax_levels["mge"] = ["id"]
        
                
        ######## OTHER
        # Keep track of which samples the data_container is holding data for
        self.sample_list = None
