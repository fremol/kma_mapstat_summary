# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 09:39:41 2020

@author: freddu
"""
''
import pandas as pd
import numpy as np
import scipy.special as sp


def clr(df):
    ''' Wrapper for CLR '''
    if _check_for_zeros(df):
        return _clr_internal(aitchison_mean(df))

    return _clr_internal(df)

def _check_for_zeros(df):
    if not df.values.all():
        print("CLR preprocessing: Dataframe contains zeros. Using Bayesian inference to replace zeros.", flush = True)
        return True
    return False

def _clr_internal(obj):
    return (np.log(obj.T) - np.mean(np.log(obj.T))).T


def aitchison_mean(df):
    ''' Return the Aitchison mean point estimate '''
    #return np.exp(sp.digamma(df+1.0)).coda.closure(1.0)
    return closure(np.exp(sp.digamma(df+1.0)), 1.0)


def closure(df, cls_const):
    ''' Apply Closure to composition '''
    return cls_const*df/(df.sum(1)[0])