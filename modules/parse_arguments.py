# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 09:59:12 2019

@author: Fredespade
"""

# Import libraries
import os
import argparse
import re
import sys
from datetime import datetime

###############################################################################
############################ PARSE INPUT ARGUMENTS ############################
###############################################################################

def argument_parser():

    # Create a parser
    args = get_args()
    
    # Check all arguments are as they should be
    args = check_args(args)

    return(args)



def get_args():

    """ 
    User input parser function.
    """
    
    # Check for hidden arguments
    if "--hidden" in sys.argv:
        
        print("\nHidden arguments:\n")
        print("-tn : test case number (how many samples to read)\n")
        print("-topm : Which measure to use with the -top arguments."\
              " Choices: relativeAbundance, fragmentCount\n")
        sys.exit(1)

    # Program description
    service_description = "kma_mapstat_summary is a CGE tool that takes"\
        " mapstat files (output from KMA) and produces produces abundance"\
        " tables. The program can integrate taxonomical annotation from"\
        " .refdata files to produce normalized abundance tables on"\
        " different taxonomical levels."

    # Create parser
    arg_parser = argparse.ArgumentParser(description=service_description)
    
    # Taxonomy levels for args.db argument
    tax_options = ["superkingdom", "phylum", "class", "order",
                   "family", "genus", "species", "strain", "database",
                   "accession", "all"]
    
    # Databases to select
    genomic_dbs = ["bacteria", "fungi", "protozoa", "parasite", "archaea",
                   "human", "imgvr", "kvit"]
    resistance_dbs = ["resfinder", "functionalresistance", "metalresistance"]
    functional_dbs = ["mge"]
    gene_16s_dbs = ["silva"]
    virus_dbs = ["imgvr", "kvit", "virus"]
    other_dbs = ["mitochondrion", "plastid"]
    database_level = ["genomic", "kvi"]
    database_options = set(genomic_dbs + resistance_dbs + functional_dbs
                           + gene_16s_dbs + virus_dbs + other_dbs + database_level)
    needs_tax = ["bacteria", "fungi", "protozoa", "parasite", "archaea",
                 "human", "kvit", "virus"]
    
    
    # Add arguments
    
    ### INPUT/OUTPUT OPTIONS
    arg_parser.add_argument("-i", metavar="MAPSTAT_FILES",
                            type=str,
                            help="Path to directory or list containing mapstat"\
                                " files for all selected databases",
                            required = True)
    
    arg_parser.add_argument("-o", metavar="OUT_PATH",
                            type=str,
                            help="Output directory path",
                            required=True)
    
    
    ### DATABASE OPTIONS
            
    arg_parser.add_argument("-db", metavar="DATABASES",
                            nargs="+", type = str.lower,
                            help="Select one or more database(s) from: %s" % database_options,
                            required = False)
    
    arg_parser.add_argument("-cdb", "--custom_db",
                            help="Select if -i points to a dir containing"\
                                " mapstat files from a database not on the"\
                                " list of available databases. This option is"\
                                " not compatible with tax annotation on higher"\
                                " than strain/species level.",
                            action="store_true",
                            required = False)
    
    arg_parser.add_argument("-tax", metavar="TAXONOMY",
                            nargs="+", type = str.lower,
                            help="Select one or more taxonomic levels to get"\
                                " output data for from: %s" % tax_options,
                            required = False)
    
    
    ### METADATA FILES

    arg_parser.add_argument("-ref", metavar="REFDATA_FILES",
                            type=str,
                            help="Path to directory or list containing"\
                                " refData files",
                            required = False)

    arg_parser.add_argument("-cnf", "--count_norm_file", metavar="COUNT_NORMALIZATION_FILE",
                            type=str,
                            help="Will scale raw fragmentCounts/fragmentCountsAln"\
                                " to the numbers in this file to obtain FPKM/TPM."\
                                " The numbers could be bacterial abundance"\
                                " f.ex.. File format: sample_id<TAB>bacterial_read_counts",
                            required = False)
        
    arg_parser.add_argument("-rf_class", metavar = "RESFINDER_CLASS_FILE",
                            type=str, 
                            help="Path to ResFinder gene class matrix. C2"\
                                " location: /home/databases/metagenomics/db/"\
                                "ResFinder_<version>/ResFinder.class",
                            default = None,
                            required = False)
    
    
    ### RAW COUNT NORMALIZATION/SCALING OPTIONS

    arg_parser.add_argument("-clr",
                            help="Select to perform CLR transformed count tables.",
                            action="store_true",
                            required = False)
    
    arg_parser.add_argument("-fpkm",
                            help="For resistance gene databases: select to"\
                                " produce FPKM-normalized tables. Requires"\
                                " argument -cnf / --count_norm_file",
                            action="store_true",
                            required = False)
    
    arg_parser.add_argument("-tpm",
                            help="For resistance gene databases: select to"\
                                " produce TPM-normalized tables. Requires"\
                                " argument -cnf / --count_norm_file",
                            action="store_true",
                            required = False)
    
    ### TOP ARGUMENTS
    
    arg_parser.add_argument("-top_a", metavar="[n]",
                            type=int, help="Top N across: Produces output for"\
                                " the top n most abundant features"\
                                " across all samples.",
                            required = False)

    arg_parser.add_argument("-top", metavar="[n]",
                            type=int,
                            help="Top N Total: Produces output for the top n"\
                                " most abundant features in total"\
                                " for each sample.",
                            required = False)
    
    ### VERBOSE AND LOGFILE SETTINGS
    
    arg_parser.add_argument("-l", metavar="LOGFILE",
                            type=str, default = None,
                            help="Path to the logfile that the script will produce.",
                            required = False)

    arg_parser.add_argument("-v",
                            default = False,
                            help="Activate verbose mode",
                            action="store_true",
                            required = False)
    
    arg_parser.add_argument("-aln",
                            default = False,
                            help="Force use of fragmentCountAln instead"\
                                " of fragmentCounts",
                            action="store_true",
                            required = False)
        
    ### SILVA DATABASE OPTIONS
    silva_options = ["bacteria", "eucaryote", "archaea"]
    arg_parser.add_argument("-ss", "--subset_silva",
                            choices=silva_options,
                            help="Extract a subset of Silva mapping results - select from: %s" % silva_options,
                            required = False)
    
    #### HIDDEN ARGUMENTS
    # Not for general use currently, in testing

    arg_parser.add_argument("-topm",
                            type=str, default = None,
                            help=argparse.SUPPRESS,
                            required = False)
            
    # Parse the args
    args = arg_parser.parse_args()
    
    # Check db arg
    if args.db == None and args.custom_db == False:
        print("\nError: One of the following options must be selected:"\
              " -db, --cdb")
        sys.exit(1)
    
    # Check logfile argument
    if args.l:
        try:
            logfile = open(args.l, "w")
        except:
            print("Could not produce logfile %s. Please specify the name and"\
                  " path of the logfile.")
            sys.exit(1)
            
        # Change output to logfile
        sys.stdout = logfile

    # Print intro
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print("\n## Program Start - local date and time: %s\n" % dt_string)
    
    # If verbose
    if args.v:
        print("# Script run as:\n%s\n " % " ".join(sys.argv))

    # Add arg database lists
    args.genomic_dbs = genomic_dbs
    args.resistance_dbs = resistance_dbs
    args.functional_dbs = functional_dbs
    args.virus_dbs = virus_dbs
    args.database_options = database_options
    args.needs_tax = needs_tax
    
    # Add tax options
    args.tax_options = tax_options
    
    # Genomic/kvi
    args.do_genomic = False
    args.do_kvi = False

    return(args)


def check_args(args):

    ########################## CHECKING DATABASE AND TAXONOMY
    
    # If verbose
    if args.v:
        print("# Validating user input...")
        
    ########################## CHECKING NORMALIZATION MEASURES
    
    if any([args.tpm, args.fpkm]) and args.custom_db:
        print("Error: argument -fpkm and -tpm cannot be used together with arg"\
              " -cdb (custom database). Continuing without FPKM/TPM normalization.")
        args.fpkm, args.tpm = False, False

    # Parse databases that user has specified
    args.db = parse_databases(args)
    
    if args.db:
        # Check imgvr
        if "imgvr" in args.db and args.ref == None:
            print("Error: database IMGVR requires .refdata file (-ref) for the"\
                  " database it belonged to (kvi/genomic)")
            sys.exit(1)
            
        if args.subset_silva and "silva" not in args.db:
            print("Error: Silva select argument (-ss) cannot be used when Silva"\
                  " is not selected as a database.")
            args.subset_silva = None
        elif args.subset_silva:
            if args.subset_silva.lower() == "bacteria":
                args.subset_silva = "Bacteria"
                print("## USING SILVA SUBSET: BACTERIA ##", flush = True)
            elif args.subset_silva.lower() == "eucaryota":
                args.subset_silva = "Eukaryota"
                print("## USING SILVA SUBSET: EUCARYOTA ##", flush = True)
            elif args.subset_silva.lower() == "archaea":
                args.subset_silva = "Archaea"
                print("## USING SILVA SUBSET: ARCHAEA ##", flush = True)
            else:
                args.subset_silva = None

    # Parse taxonomy level that user has specified
    args.taxonomy = parse_taxonomy(args, args.tax, args.ref, args.db)
        
    ########################## CHECKING FILES
        
    # Verify input .mapstat files are OK
    args.i, args.mapstat_files = find_files(args.i, args, ".mapstat")

    # Verify input .refdata files are OK
    if args.ref:
        if args.custom_db:
            print("Error: argument -ref cannot be used together with arg -cdb"\
                  " (custom database)")
            args.ref = None
            args.ref_files = None
        else:
            args.ref, args.ref_files = find_files(args.ref, args, ".refdata")
    else:
        args.ref = None
        args.ref_files = None

    # Verify input normalization files are OK
    if args.count_norm_file:
        args.count_norm_file = check_norm_file(args.count_norm_file)
    else:
        if any([args.tpm, args.fpkm]):
             raise FileNotFoundError("FPKM/TPM normalization of resistance"\
                                     " databases requires normalization file"\
                                         " (-cnf path/to/norm/file.txt).\n")
        args.count_norm_file = None
    
    # Make sure that .mapstat files, .refdata  files match
    args = match_files(args)

    # Verify if ResFinder.class file is OK
    if args.rf_class:
        if args.custom_db:
            print("Error: argument -rf_class cannot be used together with arg"\
                  " -cdb (custom database)")
            sys.exit(1)
        if "resfinder" in args.db:
            args.rf_class = find_files(args.rf_class, args, "rf_class")
        else:
            print("ResFinder.class file specified, but no resistance gene"\
                  " database specified as desired output. Ignoring argument"\
                      " -rf_class %s...\n" % args.rf_class, flush = True)
            args.rf_class = None
            
    ########################## CHECK OUTPUT FOLDER
    
    # Verify specified output directory is OK
    args.o = check_output_path(args.o)
    if not os.path.isdir(args.o):
        print("Error: the specified output directory could not be"\
              " accessed: %s." % args.o, flush = True)
        raise FileNotFoundError("Missing output directory.\nTry -h or"\
                                " --help for help.")
        
    ########################## OTHER PARAMETERS

    # Check top x argument is valid
    if args.top_a:
        if args.top_a < 0:
            raise ValueError("Error: the top_all n number must be a positive integer.")
    if args.top:
        if args.top < 0:
            raise ValueError("Error: the top n number must be a positive integer.")
        if args.v:
            print("Top: %s" % str(args.top))
            
    ########################## VERBOSE 
        
    # If verbose
    if args.v and args.db:
        l = set()
        for db in args.db:
            if db not in ["genomic_overview", "genomic"]:
                l.add(db)
            if args.do_genomic:
                l.add("genomic")
            elif args.do_kvi:
                l.add("kvi")
        print("Databases: %s" % ", ".join(l))
        
    # If verbose
    if args.v and args.taxonomy:
        l = list()
        for tax in args.taxonomy:
            if "_" in tax:
                l.append(tax.split("_")[0])
            if "#" in tax:
                l.append("accession")
        print("Taxonomy: %s" % ", ".join(l))
        
    # If verbose
    if args.v:
        print("Mapstat: %s files found" % str(len(args.mapstat_files)), flush = True)
        if args.ref:
            print("Refdata: %s" % ", ".join(args.ref_dbs), flush = True)
        if args.count_norm_file:
            print("Normalization file: %s " % args.count_norm_file\
                  .replace("\\", "/"), flush = True)
                
    # Verbose
    if args.v and args.rf_class and "resfinder" in args.db:
        print("ResFinder class metadata file: %s " % args.rf_class\
              .replace("\\", "/"), flush = True)
        
    # If verbose
    if args.v:
        print("Output path: %s " % args.o.replace("\\", "/"), flush = True)
        
    if args.v and any([args.tpm, args.fpkm, args.clr]):
        l = list()
        if args.tpm:
            l.append("TPM")
        if args.fpkm:
            l.append("FPKM")
        if args.clr:
            l.append("CLR")
        print("Normalization: %s" % ", ".join(l))
        
    if args.top_a and args.v:
        print("Top -a: %s" % str(args.top_a))
    if args.top and args.v:
        print("Top: %s" % str(args.top))
        
        
    # Check if top metric is selected
    if args.top or args.top_a:
        if args.topm:
            if args.topm == "0":
                args.topm = "relativeAbundance"
            elif args.topm == "1":
                args.topm = "fragmentCount"
            else:
                args.topm = "relativeAbundance"
    
    # Print empty
    if args.v:
        print()
    
    return(args)

def parse_databases(args):
    """ 
    Parses args.db argument.

    Makes sure the databases specified are supported, and that there is no 
    conflict between them. 

    Also checks if info from the KMA database "genomic" 
    is needed, for instance when ResFinder is specified.
    
    Currently unsupported databases that may be supported at a later date:
    fungi, virus, plasmid, protozoa,imgvr, parasites, plant, 
    invertebrates, vertebrates, archaea, virulence, greengenes

    """

    # Process args as string, removing delimiters/separation characters
    db_set = set()
    re_delimiters = " ", ",", "\t", ";"
    re_pattern = "|".join(map(re.escape, re_delimiters))
    
    if args.db == None:
        return(None)
    elif args.custom_db and args.db != None:
        print("Error: argument -db cannot be used together with arg --cdb"\
              " (custom database)")
        sys.exit(1)
    
    # Find supported databases
    for db_arg in args.db:

        # Arguments were separated with any of the above delimiters
        if any(dl in db_arg for dl in re_delimiters):
            db_split = re.split(re_pattern, db_arg)
            for db in db_split:
                if db != "":
                    if db in args.database_options:
                        db_set.add(db)
                                                
                        if db == "genomic" or db == "genomic2":
                            args.do_genomic = True
                        elif db == "kvi":
                            args.do_kvi = True

                        if db in args.genomic_dbs:
                            db_set.add("genomic")
                            db_set.add("genomic2")
                                                        
                        elif db in args.resistance_dbs and args.count_norm_file != None\
                            and any([args.tpm, args.fpkm]):
                            db_set.add("genomic_overview")
                        if db.lower() == "virus":
                            db_set.add("kvi")
                        if db in ["imgvr", "kvit"]:
                            db_set.add("kvi")
                    else:
                        print("Database %s is not a standard database." % db, flush = True)
                        print("If %s is a custom database, the"\
                              " script should be run with the -c "\
                                  "TRUE argument.\n" % db, flush = True)
                        continue
        else:           

            if db_arg in args.database_options:
                db_set.add(db_arg)
                
                if db_arg == "genomic" or db_arg == "genomic2":
                    args.do_genomic = True
                elif db_arg == "kvi":
                    args.do_kvi = True

                if db_arg in args.genomic_dbs:
                    db_set.add("genomic")
                    db_set.add("genomic2")
                elif db_arg in args.resistance_dbs and args.count_norm_file != None \
                    and any([args.tpm, args.fpkm]):
                    db_set.add("genomic_overview")
                elif db_arg in args.functional_dbs and args.count_norm_file != None \
                    and any([args.tpm, args.fpkm]):
                    db_set.add("genomic_overview")
                if db_arg.lower() == "virus":
                    db_set.add("kvi")
                if db_arg in ["imgvr", "kvit"]:
                    db_set.add("kvi")
            else:
                print("Database %s is not supported." % db_arg, flush = True)
                print("If %s is not on the list of supported database, run the"\
                      " script with argument --cdb (custom database).\n" % db_arg, flush = True)
                continue
                
    # Check if all dbs were unsupported
    if len(db_set) == 0:
        raise ValueError("Error: no supported databases specified. Try -h or"\
                         " --help for database options.")

    return(db_set)

def parse_custom_db(path):

    # Print warning to user
    print("Custom database selected.")
    print("Be aware that taxonomic aggregation will not be available.\n")
    
    return(path)


def parse_taxonomy(args, tax_arg, ref_arg, db_arg):
    """ 
    Parses the args.tax argument.

    Decides which taxonomic level the output will be based on.

    If the args.tax hasn"t been specified, it checks if there is a conflict.

    """

    # Certain databases need reference data files (.refdata)
    user_tax = None
    
    # Check if we are looking at custom database
    if args.custom_db and tax_arg != None:
        print("Error: argument -tax cannot be used together with arg -cdb"\
              " (custom database)")
        sys.exit(1)

    # Edge case: user has specified tax, but no taxonomical annotation (.refdata) files
    if tax_arg not in [None, "id"] and ref_arg == None:
        raise ValueError("Error: -tax specified above accession level, but no"\
                         " .refdata files specified. Integrating taxonomy"\
                             " requires .refdata files.")

    # Check if user forgot tax, and databases require tax
    if tax_arg == None and args.db != None:
        intersect  = list(set(db_arg).intersection(set(args.needs_tax)))
        need_tax = len(intersect) > 0
        
        if need_tax:
            raise argparse.ArgumentError(tax_arg, "Error: argument -tax"\
                                         " required for the following"\
                                        " databases: %s" % ", ".join(intersect))

    # User has specified both tax and .refdata files
    else:
        if db_arg != None:
            tax_arg = parse_raw_tax_input(args)
            
            if tax_arg == {"all"}:
                tax_arg = {"superkingdom","phylum","class",
                           "order","family","genus","species"}
                
            if "accession" in tax_arg:
                tax_arg.remove("accession")
                tax_arg.add("id")
                
            # Supported levels
            taxonomy_dict = {"superkingdom" : "superkingdom_name",
                             "phylum" : "phylum_name",
                             "class" : "class_name",
                             "order" : "order_name",
                             "family" : "family_name",
                             "genus" : "genus_name" ,
                             "species" : "species_name",
                             "strain" : "strain_name",
                             "database" : "db_name",
                             "id" : "# id"}
            user_tax = list()
            for tax in tax_arg:
                user_tax.append(taxonomy_dict[tax])
            
            # If user has chosen KVIT, we need to check what is available
            if "kvit" in db_arg or "silva" in db_arg or "kvi" in db_arg:
                print("WARNING: taxonomical annotation for some databases"\
                      " (kvit, silva, IMGVR) may not be complete, and thus"\
                          " aggregating to any level higher than feature"\
                        " ID may result in 'unknown' values being present"\
                        " in the final abundance tables.\n", flush = True)

            if args.db and any([args.do_genomic, args.do_kvi]):
                print("WARNING: certain sub-databases of kvI and Genomic"\
                      " (IMGVR) has no taxonomical annotation.", flush = True)
                print("Please be aware that many sequences from the genomic/kvI"\
                      " mapstat files will be omitted from the resulting"\
                          " abundance tables.", flush = True)
                print("In addition, the abundance tables may also contain"\
                      " sequences with 'unknown' taxonomy.\n", flush = True)
    return(user_tax)

def parse_raw_tax_input(args):
    
    # Process args as string, removing delimiters/separation characters
    tax_list = set()
    re_delimiters = " ", ",", "\t", ";"
    re_pattern = "|".join(map(re.escape, re_delimiters))
    
    # Find supported databases
    for tax_arg in args.tax:

        # Arguments were separated with any of the above delimiters
        if any(delim in tax_arg for delim in re_delimiters):
            tax_split = re.split(re_pattern, tax_arg)
            for tax in tax_split:
                if tax != "":
                    if tax in args.tax_options:
                        tax_list.add(tax)
                    else:
                        print("Taxonomy %s is not supported, skipping." % tax, flush = True)

        else:

            if tax_arg in args.tax_options:
                tax_list.add(tax_arg)

            else:
                print("Taxonomy %s is not supported, skipping." % tax_arg, flush = True)
    
    
    return(tax_list)

def find_files(input_path, args, file_type=None):
    """ 
    Takes an input path and a filetype and searches for the files of the
    given type.
    
    Returns a list of files as absolute paths as well as the input path 
    as an absolute path. 
    """
    
    # Handle if path is specified with "/drives/" format
    if input_path.startswith("/drives/"):
        input_path = re.sub(r"(/drives/)([a-z])",r"\g<2>:",input_path)
    
    # Convert to absolute path
    if not os.path.isabs(input_path):
        input_path = os.path.abspath(input_path)
        
    # Check if ResFinder.class file exists
    if file_type == "rf_class":
        if not os.path.isfile(input_path):
            raise FileNotFoundError("Specified ResFinder.class file was not"\
                                    " found: %s" % input_path)
        else:
            return(input_path)

    # Find all files of format file_type
    files_found = list()
    if file_type:

        # Specified input_path is a directory with files
        if os.path.isdir(input_path):
            for file in os.listdir(input_path):
                if file.endswith(file_type):
                    fn = os.path.abspath(input_path +"/" + file)
                    if os.path.isfile(fn):
                        files_found.append(fn)
                    else:
                        print("Unknown error occurred processing file %s in"\
                              " dir: '%s'.\nVerify that all input paths are"
                              " correct." % (file, input_path), flush = True)

        # Specified input_path is a list containing paths
        elif os.path.isfile(input_path):
            input_list = open(input_path, "r")
            for line in input_list:
                line = line.strip()
                if line.endswith(file_type):

                    # Filename matches input, convert path if needed
                    if line.startswith("/drives/"):
                        line = re.sub(r"(/drives/)([a-z])",r"\g<2>:",line)
                    if not os.path.isabs(line):
                        line = os.path.abspath(line)
                    if os.path.isfile(line):
                        files_found.append(os.path.abspath(line))
                    else:
                        print("Specified file found in input list %s could"\
                              " not be accessed:\n%s." % (input_path, line))
                        raise FileNotFoundError("Error parsing paths from"\
                                                " input list: '%s'.\nVerify"\
                                                " all paths specified in the"\
                                                " input list are correct,"\
                                                " or try specifying a directory"\
                                                " instead." % input_list)
        else:
            raise FileNotFoundError("Error parsing input: The specified list"\
                                    " or directory for %s files could not be"\
                                    " accessed:\n%s." % (file_type, input_path))

    # Check if the number of files found is zero
    if file_type != None and len(files_found) == 0:
        raise FileNotFoundError("No %s files found in specified list or"\
                                " directory %s." % (file_type, input_path))

    return(input_path, files_found)

def check_norm_file(norm_file):
    
    # Handle if path is specified with "/drives/" format
    if norm_file.startswith("/drives/"):
        norm_file = re.sub(r"(/drives/)([a-z])",r"\g<2>:",norm_file)
    
    # Convert to absolute path
    if not os.path.isabs(norm_file):
        norm_file = os.path.abspath(norm_file)
        
    if not os.path.isfile(norm_file):
        print("Error: the specified normalization count file could not be"\
              " accessed: %s." % norm_file, flush = True)
        raise FileNotFoundError("Missing normalization file.\nTry -h or"\
                                " --help for help.")
        
    else:
        try:
            lines = list()
            for line in norm_file:
                ls = line.strip().split("\t")
                lines.append(line)
        except:
            print("Error: the specified normalization file could not"\
                  " be read: %s." % norm_file, flush = True)
            raise FileNotFoundError("Missing normalization file.")


        # Only take files that match args.db
        if len(lines) == 0:
            print("Error: no data found in normalization file: %s." % \
                  norm_file, flush = True)
            raise FileNotFoundError("Missing normalization file.")
    
    return(norm_file)


def check_output_path(path):
    
    # Handle if path is specified with "/drives/" format
    if path.startswith("/drives/"):
        path = re.sub(r"(/drives/)([a-z])",r"\g<2>:",path)
    
    # Convert to absolute path
    if not os.path.isabs(path):
        path = os.path.abspath(path)
        
    return(path)


def match_files(args):

    """ Looks at names of .mapstat files, .refdata files

    In the names of .refdata files, the databases they were made
    for appear. These databases found are then compared with the databases that 
    the user specified with the args.db argument.

    If a user has specified f.ex. args.db = functionalresistance, and has 
    specified .refdata files, the program makes sure that the refdata files 
    that were specified actually match the specified database.

    If not, the program will print which databases are missing files and will
    exit.
    """

    # Compare args.db and input .mapstat to see if they match
    verified_mapstat_files = set()
    ms_dbs = set()
    db_name = ""

    for file in args.mapstat_files:
        if "__" in os.path.basename(file):
            db_name = os.path.basename(file).split("__")[0].lower().split("_")[0].lower()
        else:
            try:

                with open(file) as myfile:
                    head = [next(myfile) for x in range(5)]
    
                for line in head:
                    ls = line.strip().split()

                    if ls[0] == "##":

                        if ls[1] == "database":
                            db_name = ls[-1].lower().split("_")[0]
            except:
                continue

        # Only keep file if correct database
        if args.db:
            if db_name in args.db:
                verified_mapstat_files.add(file)
                ms_dbs.add(db_name)
        elif args.custom_db:
            verified_mapstat_files.add(file)
            ms_dbs.add(db_name)
            continue
        
    if "kvi" in ms_dbs:
        args.kvi_available = True
    else:
        args.kvi_available = False
        
    if args.db:
        if any(db in args.virus_dbs for db in args.db):
            
            if not "kvi" in ms_dbs:
                args.db.remove("kvi")
            
    # Compare args.db and input .refdata to see if they match
    if args.ref:
        verified_ref_files = set()
        ref_dbs = set()
        db_name = ""
        for file in args.ref_files:
            if "__" in os.path.basename(file):
                db_name = os.path.basename(file).split("__")[0].lower().split("_")[0].lower()
            else:
                try:

                    with open(file) as myfile:
                        head = [next(myfile) for x in range(5)]
        
                    for line in head:
                        ls = line.strip().split()

                        if ls[0] == "##":

                            if ls[1] == "database":
                                db_name = ls[-1].lower().split("_")[0]
                except:
                    continue

            # Only take files that match args.db
            if args.db:
                if db_name in args.db:
                    verified_ref_files.add(file)
                    ref_dbs.add(db_name)
                else:
                    continue

    # Args.ref dbs
    if args.ref:
        if len(ref_dbs) > 0:
            args.ref_dbs = list(ref_dbs)

    # Check if any .mapstat files left after filtering
    if len(verified_mapstat_files) == 0:
        raise ValueError("No mapstat files found matching specified databases."\
                         " Make sure databases specified are supported,"\
                        " and that mapstat files for the databases are"\
                        " in the input directory.\nTry -h or --help for"\
                        " database options.")
    elif len(verified_mapstat_files) != len(args.mapstat_files):
        args.mapstat_files = verified_mapstat_files

    # Check if there was a mismatch between .mapstat and .refdata
    if args.ref:
        difference = ms_dbs - ref_dbs
        
        if len(difference) > 0:
            raise ValueError("Error: missing .refdata files for databases %s."\
                             "\nVerify refdata files are valid for all"\
                            " specified databases." % difference)

        if len(verified_ref_files) != len(args.ref_files):
            args.ref_files = verified_ref_files
    return(args)


