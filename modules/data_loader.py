# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 10:21:41 2019

@author: Fredespade
"""

# Import libraries
import pandas as pd
import numpy as np
import os
import re
import warnings
import sys
import csv
from tqdm import tqdm

# Import from bin files
import modules.general_functions as general_functions
import modules.Mapstat as Mapstat
from modules.general_functions import prettyprint as pp

# Intercept Pandas warning as an error (when merging dataframes)
warnings.simplefilter("error", FutureWarning)

# Avoid Pandas printing warnings when slicing DataFrames
pd.options.mode.chained_assignment = None

###############################################################################
############################### READ DATA FILES ###############################
###############################################################################

def load_mapstat_files(args, data_container_obj):

    """ 
    Reads all .mapstat files.

    Stores each .mapstat file as a pandas DataFrame, and merges all DataFrames
    into one large DataFrame for each database.
    """
    
    # Make a check for virus db"s
    if args.db and "kvi" in args.db:
        genomic_check, virus_check = kvI_check(args)
    else:
        virus_check, genomic_check = False, False
        
    # Remove databases
    if virus_check:
        args.db.remove("virus")
    if genomic_check:
        args.db.remove("genomic")

    # Before we start loading data from the mapstat files, we have to check the integrity
    # of the files. This is all done in a single go, where every mapstat file is
    # made into a mapstat object which itself will check the file. What we get
    # is then lists of mapstat objects - one list for each database
    genomic_ms, resfinder_ms, functional_ms, silva_ms, kvi_ms, metal_ms,\
    mitochondrion_ms, plastid_ms, mge_ms, custom_db_ms = get_ms_lists(args, data_container_obj)
              
    # Decide how many files to process at the same time before converting to category
    if len(args.mapstat_files) > 200:
        chunk_size = 50
    else:
        chunk_size = 100
    
    try:

        if args.db:
            ####### GENOMIC
            data_container_obj.genomic_df, args.db = read_mapstat("genomic",\
                                        genomic_ms, args.db, args, chunk_size)
            # See if we had any results
            if data_container_obj.genomic_df.empty:
                args.db -= set(args.genomic_dbs)
                args.do_genomic = False
                
                if len(args.db) == 0:
                    print("Error: No more databases to process, exiting")
                    sys.exit(1)
            else:
    
                data_container_obj.genomic_df = general_functions\
                    .convert_to_category(data_container_obj.genomic_df)
            
            
            ####### SILVA
            data_container_obj.silva_df, args.db = \
                read_mapstat("silva", silva_ms, args.db, args, chunk_size)
            data_container_obj.silva_df = \
                general_functions.convert_to_category(data_container_obj.silva_df)
            
            ####### KVI
            data_container_obj.kvi_df, args.db = \
                read_mapstat("kvi", kvi_ms, args.db, args, chunk_size)
            # See if we had any results
            if data_container_obj.kvi_df.empty:
                args.db -= set(args.virus_dbs)
                args.do_kvi = False
                if len(args.db) == 0:
                    print("Error: No more databases to process, exiting")
                    sys.exit(1)
            else:
                data_container_obj.kvi_df = \
                    general_functions.convert_to_category(data_container_obj.kvi_df)
                
            ####### MITOCHONDRION
            data_container_obj.mitochondrion_df, args.db = \
                read_mapstat("mitochondrion", mitochondrion_ms, args.db, args, chunk_size)
            data_container_obj.mitochondrion_df = \
                general_functions.convert_to_category(data_container_obj.mitochondrion_df)
                    
            ####### PLASTID
            data_container_obj.plastid_df, args.db = \
                read_mapstat("plastid", plastid_ms, args.db, args, chunk_size)
            data_container_obj.plastid_df = \
                general_functions.convert_to_category(data_container_obj.plastid_df)
            
            ####### MGE
            data_container_obj.mge_df, args.db = \
                read_mapstat("mge", mge_ms, args.db, args, chunk_size)
            data_container_obj.mge_df = \
                general_functions.convert_to_category(data_container_obj.mge_df)                  
            
            ####### SPECIAL CASES #1 - resfinder
            data_container_obj.resfinder_df, args.db = \
                read_mapstat("resfinder", resfinder_ms, args.db, args, chunk_size)
            data_container_obj.resfinder_df = \
                general_functions.convert_to_category(data_container_obj.resfinder_df)
            
            ####### SPECIAL CASES #2 - functionalresistance
            data_container_obj.functional_df, args.db = \
                read_mapstat("functionalresistance", functional_ms, args.db, args, chunk_size)
            data_container_obj.functional_df = \
                general_functions.convert_to_category(data_container_obj.functional_df)
            
            ####### SPECIAL CASES #3 - metalresistance
            data_container_obj.metal_df, args.db = \
                read_mapstat("metalresistance", metal_ms, args.db, args, chunk_size)
            data_container_obj.metal_df = \
                general_functions.convert_to_category(data_container_obj.metal_df)
        
        ####### SPECIAL CASES #4 - custom db
        elif args.custom_db:
            
            if len(custom_db_ms) == 0:
                print("WARNING: No parseable mapstat files for database"\
                      " %s was found in specified mapstat dir.\n" % args.i)
                sys.exit(1)
                
            else:
                
                data_container_obj.custom_df, args.db = \
                    read_mapstat("custom", custom_db_ms, args.db, args, chunk_size)
                data_container_obj.custom_df = \
                    general_functions.convert_to_category(data_container_obj.custom_df)
                
    except:
        print("Error occurred while loading mapstat data.")
        raise

    return()

def get_ms_lists(args, data_container_obj):
    
    # Create lists to hold .mapstat files for each database
    genomic_ms = list() 
    resfinder_ms = list() 
    functional_ms = list() 
    silva_ms = list() 
    kvi_ms = list() 
    metal_ms = list() 
    mitochondrion_ms = list() 
    plastid_ms = list() 
    mge_ms = list()
    custom_db_ms = list()
    
    # Hold info
    kma_versions = list()
    samplenames = list()
    
    # Keep track of missing info
    no_features = list()
    data_missing = list()
    fragmentCount_low = list()
    
    # Get lists for each database
    if args.db:
        
        for file in args.mapstat_files:
            
            try:
                ms_file = Mapstat.Mapstat(file)
        
                if ms_file.kma_version != None:
                    kma_versions.append(ms_file.kma_version)
                if ms_file.samplename != None:
                    samplenames.append(ms_file.samplename)
            except:
                print("Error: Could not open file %s" % file)
                raise
            
            if ms_file.features_found and ms_file.data_good:
                if int(ms_file.fragmentCount_sample) < 1000000:
                    fragmentCount_low.append(ms_file)
            
                # Only save file if we are interested in the database
                if ms_file.db_name in args.db:
                
                    if ms_file.db_name == "genomic" or ms_file.db_name == "genomic2":
                        genomic_ms.append(ms_file)
                    elif ms_file.db_name == "resfinder":
                        resfinder_ms.append(ms_file)
                    elif ms_file.db_name == "functionalresistance":
                        functional_ms.append(ms_file)
                    elif ms_file.db_name == "metalresistance":
                        metal_ms.append(ms_file)
                    elif ms_file.db_name == "silva":
                        silva_ms.append(ms_file)
                    elif ms_file.db_name == "kvi":
                        kvi_ms.append(ms_file)
                    elif ms_file.db_name == "mitochondrion":
                        mitochondrion_ms.append(ms_file)
                    elif ms_file.db_name == "plastid":
                        plastid_ms.append(ms_file)
                    elif ms_file.db_name == "mge":
                        mge_ms.append(ms_file)
                    
            else:
                if not ms_file.features_found:
                    no_features.append(file)
                    
                elif not ms_file.data_good:
                    data_missing.append(file)
                    
    elif args.custom_db:
        args.db = list()
        for file in args.mapstat_files:
            
            try:
                ms_file = Mapstat.Mapstat(file)
        
                if ms_file.kma_version != None:
                    kma_versions.append(ms_file.kma_version)
                if ms_file.samplename != None:
                    samplenames.append(ms_file.samplename)
                if int(ms_file.fragmentCount_sample) < 1000000:
                    fragmentCount_low.append(ms_file)
            except:
                print("Error: Could not open file %s" % file)
            
            if ms_file.features_found and ms_file.data_good:
            
                # Only save file if we are interested in the database
                custom_db_ms.append(ms_file)
                    
            else:
                if not ms_file.features_found:
                    no_features.append(file)
                    
                elif not ms_file.data_good:
                    data_missing.append(file)
                
    # Check if any files had zero features aka no results
    if len(no_features) > 0:
        try:
            f = args.o.replace("\\", "/") + "/empty_mapstat_files.txt"
            with open(f, "w") as fh:
                for file in no_features:
                    fh.write(file + "\n")
            print("WARNING: Empty mapstat file(s) detected. A list of the"\
                  " empty files has been written to %s.\n" % f, flush = True)
        except:
            print("WARNING: Empty mapstat file(s) detected. These files"\
                  " have been skipped in the analysis.\n", flush = True)
            
    # Check if any files were missing data
    if len(data_missing) > 0:
        try:
            f = args.o.replace("\\", "/") + "/mapstat_with_EOF_error.txt"
            with open(f, "w") as fh:
                for file in data_missing:
                    fh.write(file + "\n")
            print("WARNING: Mapstat file(s) detected with a wrong EOF format,"\
                  " suggesting missing data at the end of the file. A list of"\
                " the relevant files has been written to %s.\n" % f, flush = True)
        except:
            print("WARNING: Mapstat file(s) detected with a wrong EOF format,"\
                  " suggesting missing data at end of the file. These files"\
                " have been skipped in the analysis.\n", flush = True)
    
    # Check for low fragmentcounts
    if len(fragmentCount_low) > 0:
        print("WARNING: low fragmentCount/sequencing depth detected for samples:")
        for ms_file in fragmentCount_low:
            print("%s fragmentCount: %s" %(ms_file.samplename, ms_file.fragmentCount_sample))
        print("Low sequencing depth may be due to older sequencing platforms."\
              " It is advisable to verify the numbers before using results"\
            " obtained from this analysis.\n")
    
    # Check KMA versions  
    if len(set(kma_versions)) > 1:
        
        if not args.l:
            # Multiple versions of KMA were used in the production of the mapstat files.
            print("WARNING: Multiple KMA versions detected in input mapstat"\
                  " files: %s. This may affect mapping results." \
                      % str(set(kma_versions)), flush = True)
            abort = input("Would you like to continue execution? y/n : \n")
            
            if abort.lower() in ["no", "n", "0"]:
                print("Information about the different versions of KMA can"\
                      " be found at https://bitbucket.org/genomicepidemiology"\
                        "/kma/commits/all.\n", flush = True)
                sys.exit(1)
        else:
            print("WARNING: Multiple KMA versions detected in input mapstat"\
                  " files: %s. This may affect mapping results.\n" \
                      % str(set(kma_versions)), flush = True)
            
    # Store names of all samples in Data_container object
    data_container_obj.sample_list = set(samplenames)
            
    return(genomic_ms, resfinder_ms, functional_ms, silva_ms, kvi_ms, \
           metal_ms, mitochondrion_ms, plastid_ms, mge_ms, custom_db_ms)


def load_refdata_files(args, data_container_obj, base_output):
    """ 
    Adds reference data from .refdata files to the .mapstat data.

    Reads .refdata files into DataFrames, and merges them with existing 
    DataFrames containing .mapstat data.

    The resulting merged DataFrame is stored in the Data_container object.
    """
    
    if args.taxonomy != None:
        tax_levels = args.taxonomy.copy()
        if "# id" in tax_levels:
            tax_levels.remove("# id")
        if "superkingdom_name" in tax_levels:
            tax_levels.remove("superkingdom_name")

    # Read all .refdata files
    for file in args.ref_files:

        # Extract info from filename
        fname = os.path.basename(file)
        db_name = fname.split("_")[0].lower()
        db_version = fname.split(".")[0]

        if db_name in args.db:
            try:
                
                ##### NON FUNCTIONAL DATABASES
                
                if "genomic" == db_name or "genomic2" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    if args.taxonomy != None:    
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"] + tax_levels
                    else:
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"]
                        
                    # Read in genomic refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    
                    # Remove any "NULL" values (which constitutes unknown 
                    # taxonomy) and replace with "unknown taxonomy"
                    refdata.replace(np.nan, "unknown", regex=True)
                    
                    # Edit the id 
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.genomic_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.genomic_df, "genomic", refdata,"id", "id", ".refdata", base_output))
                    data_container_obj.genomic_has_ref = True
                    
                elif "silva" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    if args.taxonomy != None:    
                        columns = ["# id", "id_len", "superkingdom_name"] + tax_levels
                    else:
                        columns = ["# id", "id_len", "superkingdom_name"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    
                    # Remove any "NULL" values (which constitutes unknown taxonomy) and replace with "unknown taxonomy"
                    refdata = refdata.replace(np.nan, "unknown", regex=True)
                    
                    # Edit the id 
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.silva_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.silva_df, "silva", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.silva_has_ref = True
                    
                    
                elif "kvi" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    if args.taxonomy != None:    
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"] + tax_levels
                    else:
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    
                    # Remove any "NULL" values (which constitutes unknown taxonomy) and replace with "unknown taxonomy"
                    refdata = refdata.replace(np.nan, "unknown", regex=True)
                    
                    # Edit the id 
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.kvi_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.kvi_df, "kvi", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.kvi_has_ref = True
                    
                elif "mitochondrion" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    if args.taxonomy != None:    
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"] + tax_levels
                    else:
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    
                    # Remove any "NULL" values (which constitutes unknown taxonomy) and replace with "unknown taxonomy"
                    refdata = refdata.replace(np.nan, "unknown", regex=True)
                    
                    # Edit the id 
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.mitochondrion_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.mitochondrion_df, "mitochondrion", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.mitochondrion_has_ref = True
                    
                elif "plastid" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    if args.taxonomy != None:    
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"] + tax_levels
                    else:
                        columns = ["# id", "id_len", "db_name", "superkingdom_name"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    
                    # Remove any "NULL" values (which constitutes unknown taxonomy) and replace with "unknown taxonomy"
                    refdata = refdata.replace(np.nan, "unknown", regex=True)
                    
                    # Edit the id 
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.plastid_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.plastid_df, "plastid", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.plastid_has_ref = True

                elif "mge" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    columns = ["# id", "id_len"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
                    
                    # Read in resfinder refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.mge_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.mge_df, "mge", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.MGE_has_ref = True    

                #### FUNCTIONAL DATABASES

                elif "resfinder" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    columns = ["# id", "id_len"]
                    
                    # We only need a few columns
                    #columns = [0, 1]
                    
                    # Read in resfinder refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.resfinder_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.resfinder_df, "resfinder", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.resfinder_has_ref = True

                elif "functionalresistance" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    columns = ["# id", "id_len"]
                    
                    # We only need a few columns
                    columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.functional_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.functional_df, "functionalresistance", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.functionalresistance_has_ref = True
                    
                elif "metalresistance" == db_name:
                    
                    if args.v:
                        print("# Loading .refdata file for database %s...\n" % db_version, flush = True)
                    
                    # Select columns we need
                    columns = ["# id", "id_len"]
                    
                    # We only need a few columns
                    columns = [0, 1]
        
                    # Read in functional resistance refdata
                    refdata = pd.read_csv(file, sep = "\t", skiprows = 1, usecols = columns)
                    refdata["id"] = refdata["# id"]
                    refdata.drop(columns = ["# id"])
                    refdata = refdata.set_index("id")
        
                    # Merge with existing df
                    data_container_obj.metal_df = \
                        general_functions.convert_to_category(general_functions.\
                        merge_dataframes(data_container_obj.metal_df, "metalresistance", refdata, "id", "id", ".refdata", base_output))
                    data_container_obj.metalresistance_has_ref = True
                    
            except UserWarning:
                print("Error reading .refdata file: %s" % fname)
                raise

            except:
                print("Unknown error parsing .refdata file '%s':" % (fname))
                raise

    return()

def load_resfinder_class_metadata(args_rf_class, data_container_obj, args_o, maindir):
    
    """
    Adds metadata (class) for ResFinder genes from the ResFinder.class file.
    
    If no file has been specified, it takes the version located in the data/ folder.
    
    """
    
    resfinder_genes = set(data_container_obj.resfinder_df["id"])

    try:
        
        # Open class matrix
        class_file = open(args_rf_class, "r")
        
        # Read the header
        class_pos = dict()
        header = class_file.readline().strip().split("\t")
        for i, geneclass in enumerate(header[1:]):
            
            if geneclass.lower() == "missing":
                class_pos[i] = "unknown class"
            else:
                class_pos[i] = geneclass.lower()
            
        # Read the classes
        class_dict = dict()
        metadata_genes = set()
        for line in class_file:
            ls = line.strip().split("\t")
            
            gene = ls[0]
            metadata_genes.add(gene)
            
            
            for i, num in enumerate(ls[1:]):
                if str(num) == "1":
                    if gene in class_dict:
                        class_dict[gene] += ", " + str(class_pos[i]) 
                    else:
                        class_dict[gene] = class_pos[i]
                        
            if gene in class_dict:
                if class_dict[gene] == "":
                    class_dict[gene] = "unknown class"
            else:
                class_dict[gene] = "unknown class"
                
        # See how many genes we are missing data for
        resfinder_genes = set(data_container_obj.resfinder_df["id"])
        diff = resfinder_genes - metadata_genes
        
        

        # If diff > 0, then warn user
        if len(diff) > 0:
            print("WARNING: missing class annotation for %s genes in the"\
                  " ResFinder.class file." % str(len(diff)), flush = True)
            print("These genes will be classified as 'unknown class'"\
                  " in the _class_ abundance tables.", flush = True)
    
            try:
                if os.path.isdir(args_o):
                    path = args_o + "/" + "resfinder_IDs_without_class_annotation.txt"
                else:
                    path = maindir + "/" + "resfinder_IDs_without_class_annotation.txt"
                    
                missing_class = data_container_obj.resfinder_df[data_container_obj.resfinder_df["id"].isin(diff)]
                missing_class.to_csv(path, sep = "\t", quoting=csv.QUOTE_NONE)
                print("\nA list of the genes can be seen in:\n%s" % path.replace("\\", "/"))
            except:
                print("Error: tried writing a list of genes with missing class"\
                      " annotation, but failed for unknown reasons.", flush = True)
                    
            print("\nIf you wish to avoid this error in the future, make sure"\
                  " to specify -rf_class with the newest version of the"\
                " ResFinder.class file (usually found under"\
                " /home/databases/metagenomics/db/ResFinder_"\
                "<version_number>/ResFinder.class).\n", flush = True)
        
        # Set class to "unknown class" for the missing genes
        for gene in resfinder_genes:
            
            if gene not in class_dict.keys():
                class_dict[gene] = "unknown class"

        # Remap class values in pandas column using the class_dict
        select = data_container_obj.resfinder_df["id"].isin(class_dict.keys())
        data_container_obj.resfinder_df.loc[select, "class"] = \
            data_container_obj.resfinder_df.loc[select, "id"].map(class_dict)
        data_container_obj.resfinder_df["class"] = \
            data_container_obj.resfinder_df["class"].astype("category")
        data_container_obj.resfinder_df = \
            general_functions.convert_to_category(data_container_obj.resfinder_df)
        
        class_file.close()
    except:
        print("Unexpected error parsing %s in modules/data_loader.py."\
              % args_rf_class, flush = True)
        print("Make sure the file is correctly formatted.", flush = True)
        raise
    
    return()

def read_mapstat(database, mapstat_list, args_db, args, chunk_size):

            
    # Check if we even have database in out args_db
    if database in args_db or args.custom_db:
        
        # Check if the mapstat list is empty
        if len(mapstat_list) == 0:
            print("WARNING: No parseable mapstat files for database %s was"\
                  " found in specified mapstat dir - skipping database...\n"\
                      % database.capitalize())
            args_db.remove(database)

            # Exit program if there are no more databases to process
            if len(args_db) == 0:
                print("Error: No more databases to process, exiting")
                sys.exit(1)
            else:
                return(pd.DataFrame(), args_db)
            
        else:
            
            # Figure out which columns in mapstat to use            
            # Is a bit tricky - depends on database and user choices. 
            # The args.aln argument always takes precedence however
            if args.aln:
                need_cols = ["# refSequence", "fragmentCountAln", "fragmentCount"]
            else:
                if "fragmentCountAln" in mapstat_list[0].header:
                    need_cols = ["# refSequence", "fragmentCount", "fragmentCountAln"]
                else:
                    need_cols = ["# refSequence", "fragmentCount"]
            
            # Print verbose status
            if args.v:
                print("# Loading data: %s %s mapstat files..."\
                      % (len(mapstat_list), database.capitalize()), flush = True)
            
            # Now parse mapstat files
            dfs_outer = list()
            i = 0
            for chunk in divide_chunks(mapstat_list, chunk_size):
                dfs_inner = list()
                for mapstat in chunk:
                    
                    # Print verbose progress
                    i += 1
                    if args.v:
                        general_functions.print_progress(i, len(mapstat_list)\
                                            ,"Loading mapstat data: ", "", 1, 50)
                    
                    
                    # Read data from mapstat
                    try:
                        df = reader_pandas(mapstat.file, need_cols)
                        
                        
                        # SPECIAL CASE #1 - resfinder
                        if database == "resfinder":
                                                            
                            split_refsequence = \
                                df["# refSequence"].str.split(" ", n = 3, expand = True)
                            df["id"] = split_refsequence.iloc[:,0]
                            
                            # Fix the resfinder ID"s that have an "original_name" annotation
                            idx_name = dict()
                            for index, row in split_refsequence.iterrows():
                                try:
                                    if "original_name" in row.iloc[2]:
                                        org_name = row.iloc[2].split("original_name=")[-1]
                                        idx_name[index] = org_name
                                except:
                                    continue
                            for idx in idx_name:
                                df["id"].iloc[idx] = idx_name[idx]
                            
                            df = df.drop(columns = ["# refSequence"])
                            df["sample"] = mapstat.samplename
                            df["sample_fragmentCount"] = mapstat.fragmentCount_sample
                            
                        # SPECIAL CASE #1 - functionalresistance
                        elif database == "functionalresistance":
                            
                            df["class"] = \
                                df["# refSequence"].str.split("|", n = 1, expand = True).iloc[:,0]
                            df.rename(columns={"# refSequence" : "id"}, inplace = True)
            
                            # Dictionary with information about ARD families
                            ARD_familiy_dict = {
                                "16S_rRNA_methyltransferase" : "16S rRNA methyltransferase for aminoglycoside resistance",
                                "AAC" : "aminoglycosides acetyl-transferases",
                                "ANT" : "aminoglycosides nucleotidyl-transferases",
                                "APH" : "aminoglycosides phospho-transferases",
                                "beta_lactamase" : "beta-lactamases (all sub-families)",
                                "cat" : "chloramphenicol acetyl-transferases",
                                "dfr" : "dihydrofolate reductase",
                                "efflux_pump" : "efflux pumps",
                                "qnr" : "quinolone resistance",
                                "tet_efflux" : "spanning Tet(A) and variants",
                                "tet_protection" : "spanning Tet(M) and variants",
                                "tetX" : "tetracycline monooxygenases",
                                "van_ligase" : "homologues of D-Ala-D-x ligases (selected on D-cycloserine)"}
                             
                            df["class"] = df["class"].map(ARD_familiy_dict).fillna(df["class"])
                            df["sample"] = mapstat.samplename
                            df["sample_fragmentCount"] = mapstat.fragmentCount_sample

                        # SPECIAL CASE #3 - metalresistance
                        elif database == "metalresistance":
                            
                            split_refsequence = df["# refSequence"].str.split(" ", n = 1, expand = True)
                            df["class"] = split_refsequence.iloc[:,1]
                            df["id"] = split_refsequence.iloc[:,0]
                            df = df.drop(columns = ["# refSequence"])
    
                            df["sample"] = mapstat.samplename
                            df["sample_fragmentCount"] = mapstat.fragmentCount_sample
                            
                        # SPECIAL CASE #4 - custom db
                        elif database == "custom":
                            
                            df["id"] = df["# refSequence"]
                            df = df.drop(columns = ["# refSequence"])
                            df["sample"] = mapstat.samplename
                            df["sample_fragmentCount"] = mapstat.fragmentCount_sample
                                
                        # GENERAL CASE - most databases (genomic, silva, kvi, mitochondria, plastid, mge, etc)
                        else:
                            df["id"] = df["# refSequence"].str.split(" ", n = 1, expand = True).iloc[:,0]
                            df = df.drop(columns = ["# refSequence"])
                            df["sample"] = mapstat.samplename
                            df["sample_fragmentCount"] = mapstat.fragmentCount_sample
                            
                        if not df.empty:
                                dfs_inner.append(df)
    
                    except:
                        print("Could not parse information from mapstat file %s. Skipping..." % mapstat.file)
                        continue
                # Concat to a single list, reduce memory
                df_inner = pd.concat(dfs_inner)
                df_inner = general_functions.convert_to_category(df_inner)
                dfs_outer.append(df_inner)
            # Concat sub-lists to a large list, reduce memory
            concat_df = pd.concat(dfs_outer)
            concat_df = general_functions.convert_to_category(concat_df)
            
            # Print verbose status
            if args.v:
                features_in_files = len(set(concat_df["id"]))
                print("Number of features found in %s mapstat files: %s\n" % (database.capitalize(), features_in_files))
                
            return(concat_df, args_db)
        
    else:
        return(pd.DataFrame(), args_db)
                
    return(pd.DataFrame(), args_db)


def load_count_file(norm_file, args_db, data_container_obj, args_o):
    """ 
    Adds count dat from count file
    """
    
    # Make normalization count Dataframe
    data_container_obj.norm_dataframe = pd.DataFrame()

    try:
        
        # Read in normalization data
        norm_data = pd.read_csv(norm_file, sep = "\t", skiprows = 0, names = ["sampleId", "norm_count"], dtype = {"norm_count" : "uint32"})
        
        # Check if empty
        if len(norm_data.index) == 0:
            print("Error: normalization file is empty: %s" % norm_file)
            sys.exit(1)

        # Add to data_container
        data_container_obj.norm_dataframe = norm_data
        data_container_obj.has_norm = True
        data_container_obj.norm_file = norm_file

    except:
        print("Error: could not load normalization file %s." % norm_file)
        print("Make sure the normalization file is in the following format: sampleID<TAB>count")
        print("Example:\nDTU2021_SAMPLE_1\t1234567\nDTU2021_SAMPLE_2\t1234567\nDTU2021_SAMPLE_3\t1234567\n")
        raise

    return()

def reader_pandas(filename, select_cols):
    return pd.read_csv(filename, sep = "\t", skiprows = 6, usecols = select_cols, skip_blank_lines = True, engine = "c", na_filter = False, low_memory = True)

def divide_chunks(inputlist,n):
    for i in range(0, len(inputlist), n):
        yield(inputlist[i:i+n])
        
def kvI_check(args):
    
    remove_genomic = False
    remove_virus = False
    
    # Get list of genomic dbs without virus dbs
    gen_dbs = set(args.genomic_dbs) - set(args.virus_dbs)

    # Check if we need to remove genomic
    genomic_check = any(db in gen_dbs for db in args.db)
    
    if not genomic_check and "genomic" in args.db:
        # Remove "genomic" from dbs to process, if we have kvI files 
        # and there are no other dbs that require genomic
        if args.kvi_available and not args.do_genomic:
            remove_genomic = True
            
    # Check for virus db
    if "virus" in args.db and not args.kvi_available:
        print("kvI mapstat files not found, skipping database Virus...\n")
        remove_virus = True
    
    return(remove_genomic, remove_virus)
