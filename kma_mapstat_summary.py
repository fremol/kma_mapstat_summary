#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 14:14:31 2019
@author: Frederik Duus Møller
e-mail: frederikduusmoeller@gmail.com

kma_mapstat_summary is a CGE tool that takes .mapstat files (output from KMA
and produces summarized abundance tables. The program can integrate taxonomical
annotation from .refdata files .refdata files to produce taxonomically
annotated tables.

In addition the script can produced normalized tables, using normalization
such as FPKM, TPM or CLR.

Source code:
  - https://bitbucket.org/fremol/kma_mapstat_summary/src/


This file is the main program run on the command line. It uses the scripts in
the /modules/ directory to run.
"""

# Import libraries
import os
import gc
import sys
import argparse
import traceback
import shutil
from datetime import datetime
import pandas as pd

# Import from bin files
import modules.parse_arguments as parse_arguments
import modules.data_loader as data_loader
import modules.general_functions as general_functions
import modules.mapstat_summary as mapstat_summary
import modules.Data_container as Data_container
from modules.general_functions import prettyprint as pp

# Avoid Pandas printing warnings when slicing DataFrames
pd.options.mode.chained_assignment = None

###############################################################################
################################# MAIN METHOD #################################
###############################################################################

def main():
    """
    Main function of tool.

    """

    # Make sure user is running the correct version of python
    try:
        assert sys.version_info[0] == 3
    except Exception:
        print("Error: wrong version of Python detected. Make sure this script \
              is run using Python 3.")
        sys.exit()

    # Parse input arguments
    try:
        args = parse_arguments.argument_parser()
    except FileNotFoundError as e:
        print("FileNotFoundError occurred while parsing user input arguments.")
        print("{0}".format(e))
        sys.exit(1)
    except ValueError as e:
        print("ValueError occurred while parsing user input arguments.")
        print("{0}".format(e))
        sys.exit(1)
    except AttributeError as e:
        print(traceback.print_exc())
        print("AttributeError occurred while parsing user input arguments.")
        print("{0}".format(e))
        sys.exit(1)
    except argparse.ArgumentError as e:
        print("argparse.ArgumentError occurred while parsing user input arguments.")
        print("{0}".format(e))
        sys.exit(1)
    except Exception:
        print("Unexpected error occurred while trying to parse user input arguments.")
        print(traceback.print_exc())
        sys.exit(1)

    # Create data_container object. Organizes data for easy access.
    data_container_obj = Data_container.Data_container()

    # Estimate workload to determine if user will need to take a nap before script finishes
    general_functions.estimate_workload(args)

    # Create output directory
    try:
        base_output = args.o + "/" + "kma_mapstat_summary_output"
        args.output_dirs, args.created_dirs = general_functions.create_output_folder(base_output)
    except Exception:
        print("Error occurred while trying to create output directory.")
        raise

    # Read .mapstat files
    print("######################################\n")
    print("## Reading mapstat files...\n", flush = True)
    try:
        data_loader.load_mapstat_files(args, data_container_obj)
        gc.collect()
    except pd.errors.EmptyDataError as e:
        print("EmptyDataError occurred while parsing .mapstat files.")
        print("{0}".format(e))
        sys.exit(1)
    except ValueError as e:
        print(traceback.print_exc())
        print("ValueError occurred while parsing .mapstat files.")
        print("{0}".format(e))
        sys.exit(1)
    except StopIteration as e:
        print("StopIteration occurred while parsing .mapstat files.")
        print("{0}".format(e))
        sys.exit(1)
    except AttributeError as e:
        print("AttributeError occurred while parsing .mapstat files.")
        print("{0}".format(e))
        sys.exit(1)
    except Exception:
        print("Unexpected error occurred while trying to parse .mapstat files.")
        print(traceback.print_exc())
        sys.exit(1)
    
    # Read .refdata files
    if args.ref:
        print("######################################\n")
        print("## Reading refdata files...\n", flush = True)
        try:
            data_loader.load_refdata_files(args, data_container_obj, base_output)
            gc.collect()
        except pd.errors.EmptyDataError as e:
    
            print("EmptyDataError occurred while trying to parse .refdata files.", flush = True)
            print("{0}".format(e))
            sys.exit(1)
        except UserWarning as e:
            print("UserWarning occurred while trying to parse .refdata files.", flush = True)
            print("{0}".format(e))
            sys.exit(1)
        except Exception:
            print("Unexpected error occurred while trying to parse .refdata files.", flush = True)
            print(traceback.print_exc())
            sys.exit(1)
            
    # Parse ResFinder.class file
    if args.db and "resfinder" in args.db and not data_container_obj.resfinder_df.empty:
        print("######################################\n")
        print("## Reading ResFinder.class file...\n", flush = True)
        try:
            
            if args.v and not args.rf_class:
                print("No ResFinder.class file (arg -rf_class) specified."\
                      " Classes taken from default file (kma_mapstat_summary/"\
                          "data/resfinder_metadata).", flush = True)
                print("Be aware that this may mean that some genes annotations"\
                      " are not up to date with the newest version of the ResFinder"\
                          " database.\n", flush = True)
                print("The newest version of the file can usually be found"\
                      " under /home/databases/metagenomics/db/ResFinder_"\
                          "<VERSION>/ResFinder.class on Computerome 2.")
                print()
            dirname = os.path.dirname(os.path.abspath(__file__))
            if args.rf_class:
                data_loader.load_resfinder_class_metadata(args.rf_class, data_container_obj, base_output, dirname)
            else:
                rf_class = os.path.join(dirname,"data","ResFinder.class")
                data_loader.load_resfinder_class_metadata(rf_class, data_container_obj, base_output, dirname)
        except Exception:
            print("Unexpected error occurred while trying to parse ResFinder.class file.", flush = True)
            print(traceback.print_exc())
            sys.exit(1)
                
    # Read genomic .overview file
    if args.count_norm_file:
        print("######################################\n")
        print("## Reading count normalization file...\n", flush = True)
        try:
            data_loader.load_count_file(args.count_norm_file, args.db, data_container_obj, base_output)
            gc.collect()
        except NameError as e:
            print("NameError occurred while trying to parse norm count file.", flush = True)
            print("{0}".format(e))
            sys.exit(1)
        except Exception:
            print("Unexpected error occurred while trying to parse norm count file.", flush = True)
            print(traceback.print_exc())
            sys.exit(1)
        
    # Make output dirs
    try:
        print("######################################\n")
        print("## Creating output directories...\n", flush = True)
        args.output_dirs, args.created_dirs = general_functions.generate_output_dirs(args, base_output, data_container_obj)
    except FileNotFoundError as e:
        print("FileNotFoundError occurred while trying to create output directories.")
        print("{0}".format(e))
        cleanup(args.created_dirs)
        sys.exit(1)
    except OSError as e:
        print("OSError occurred while trying to create output directories.")
        print("The system is likely trying to delete folders that aren't empty:")
        print("{0}".format(e))
        cleanup(args.created_dirs)
        sys.exit(1)
    except Exception:
        print("Unexpected error occurred while trying to create output directories.")
        print(traceback.print_exc())
        cleanup(args.created_dirs)
        sys.exit(1)

    # Check if we can perform TPM or FPKM normalization (if we have necessary data)
    if args.tpm:
        args.tpm = general_functions.parse_normalization_measures("TPM", args.db, data_container_obj)
    if args.fpkm:
        args.fpkm = general_functions.parse_normalization_measures("FPKM", args.db, data_container_obj)
    
    # Calculate stats
    print("######################################\n")
    print("## Producing abundance tables...\n", flush = True)
    mapstat_summary.run_mapstat_parser(args, data_container_obj)
    
    # End program
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print("######################################\n")
    print("## Program End - local date and time: %s\n" % dt_string)
    
    return()

###############################################################################
########################### HOUSEKEEPING FUNCTIONS ################S############
###############################################################################
    
def cleanup(created_dirs):
    ''' Removes output directories in case that script was terminated unsuccesfully '''
    
    # Remove all created dirs
    if len(created_dirs) == 0:
        return()
    elif "mainout" in created_dirs:
        shutil.rmtree(created_dirs["mainout"])
    else:
        for folder in created_dirs:
            if os.path.isdir(created_dirs[folder]):
                try:
                    os.rmdir(created_dirs[folder])
                except:
                    print("Error removing directory %s for %s" % (folder, created_dirs[folder]))
                    raise
                    continue 
    return()

if __name__ == "__main__":
    main()
    